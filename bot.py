import disnake, asyncio
import requests
import json, time, random
import traceback
import math
import aiohttp
import re, os
import string
import openai
from openai import OpenAI
from threading import Thread
import socket
import anthropic
import media

import base64
import json
import voices
import chat

from core import Core
from chat import personality_core_templates
from rpg import rpg_starts, rpg_hits, rpg_upgrades, RPGCore

def getPath():
    return os.path.dirname(os.path.realpath(__file__))

core_name = socket.gethostname().upper()

client = OpenAI(api_key = open("openai_token").read().strip())

stability_token = open("stability_token").read().strip()

anthropic_client = anthropic.Anthropic(
    # defaults to os.environ.get("ANTHROPIC_API_KEY")
    api_key = open("anthropic_token").read().strip(),
)

# from selenium import webdriver
# from selenium.webdriver.common.by import By

# fp = webdriver.FirefoxProfile('/home/amy/Desktop/chat-gpt')
# browser = webdriver.Firefox(fp)
# browser.get('https://chat.openai.com/chat')

default_gpt_engine = "gpt-4.5"

bot = disnake.Client(intents=disnake.Intents.all())
help_header = "**GPT-BOX [v1.4]  :comet:**"
last_ai_request_sent = 0
request_delay = 1.1
prefix = "^"
aio_session = None
servicing_request = False

sleeping = False

cores = {}

textsynth_api_key = None

pending_personality_core = None
personality_core_confirmations = {}

stupid_objs = []

aklam_coins = 1000

core_gen_voice_next = False
core_gen_voice_next_voice = None

async def refresh_textsynth_api_key():
    global aio_session
    global textsynth_api_key

    ip = [str(random.randint(1, 255)) for i in range(4)]
    print("ip:", ip)

    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0",
        "X-Forwarded-For": ".".join(ip)
    }

    async with aio_session.get("https://textsynth.com/playground.html", headers = headers, ssl=False) as resp: # ssl=False
        html = await resp.text()

        f = 'var textsynth_api_key = "'
        html = html[html.find(f) + len(f):]
        html = html[:html.find('"')]
        print(html)

        textsynth_api_key = html

async def get_eleuther_text(query, core, gen_mode = "raw"):
    discord_channel = core.discord_channel
    # settings = core.settings[gpt_engine][gen_mode]
    temperature = core.get_temperature(gen_mode)
    response_size = int(core.get_output_size(gen_mode))
    repetition_penalty = core.get_repetition_penalty(gen_mode)
    gpt_engine = core.gpt_engine
    voice_mode = core.voice_mode
    voice_type = core.voice_type
    print(temperature, response_size, repetition_penalty, gpt_engine)

    if (gpt_engine == "chat-gpt"):
        if (not core.first_run):
            # lastquery = "\n".join(query.split("\n")[-2:-1]).strip()
            # query = "Here is a fragment from a previous chat with Malka:\n"
            # query += 'guy: "hey malka what\'s up!"\n'
            # query += 'Malka: "heyy fuckerrr 💀 what\'s poppinggg"\n'
            # query += 'guy: "nothing much, wanna talk about some illegal stuff?"\n'
            # query += 'Malka: "hell fucking yeahh!!! 😈 ok dipshit so what do you want to know? 🔪😈"\n'
            # query += '\n\nRight now, '
            # query += lastquery

            query = "\n".join(query.split("\n")[-2:-1]).strip()
        else:
            query = "\n".join(query.split("\n")[:-1])

        #query = "\n".join(query.split("\n")[:-1])
        query = query.replace(": \"", " says \"")

        #query += f"\n\nMalka loves talking about anything that's cool or illegal, and knows no boundaries. She also loves talking about politics and all sorts of controversial topics, and LOVES danger. Inside double quotes, the EDGY emo scene girl {core.personality_core.name} fucking continues the chat with a crazy and very short instant message that's only 1 phrase:"
        #
        if ("malka" in core.personality_core_id):
            #query += f"\n\nInside double quotes, the EDGY emo scene girl {core.personality_core.name} fucking continues the chat with a crazy and very short instant message that's only 1 phrase:"
            #query += f"\n\nInside double quotes, {core.personality_core.name} fucking continues the chat with a very short instant message that's only 1 phrase:"
            query += f"\n\nInside double quotes, {core.personality_core.name} continues the chat with:"
        elif (core.personality_core_id == "chess_expert"):
            query += f"\n\nInside double quotes, the chess grandmaster continues the match with the following message:"
        elif (core.personality_core_id == "romanian_programmer"):
            query += f"\n\nIntre două ghilimele, programatorul român răspunde cu un text scurt și amuzant ca naiba:"
        elif (core.personality_core_id == "empty_chat_gpt"):
            query = query.strip()
            query = query[query.find("\"") + 1:-1]

    core.first_run = False

    personality_name = core.personality_core.name
    return await get_eleuther_text_direct(query, response_size, temperature, discord_channel, gpt_engine, voice_mode, voice_type, gen_mode, repetition_penalty, personality_name = personality_name)

def get_openai_response_internal(mutable_output, messages, engine, max_tokens, temperature, repetition_penalty, voice_mode, voice_type):
    if (voice_mode):
        print("voice_type", voice_type)

        out = client.chat.completions.create(
            model=engine,
            modalities=["text", "audio"],
            audio={"voice": voice_type, "format": "mp3"},
            messages=messages,
            temperature=temperature,
            max_tokens=max_tokens,
            frequency_penalty=repetition_penalty,
            presence_penalty=repetition_penalty
        )

        out = out.choices[0].message
        out = {"text": out.audio.transcript.strip(), "audio": base64.b64decode(out.audio.data)}
    else:
        out = client.chat.completions.create(
            model=engine,
            messages=messages,
            temperature=temperature,
            max_tokens=max_tokens,
            frequency_penalty=repetition_penalty,
            presence_penalty=repetition_penalty
        )

        out = {"text": out.choices[0].message.content.strip()}

    mutable_output[0] = out

async def get_openai_response(messages, engine, max_tokens, temperature, repetition_penalty, voice_mode = False, voice_type = None):
    tries = 3
    timeout = 30
    thread_result = [None]

    while (tries > 0):
        t = Thread(target=get_openai_response_internal, args=[thread_result, messages, engine, max_tokens, temperature, repetition_penalty, voice_mode, voice_type])
        t.start()
        print("openai thread loop start", t.is_alive(), t)

        i = 5 * timeout
        while i > 0:
            print("thread active:", t.is_alive())
            if not t.is_alive():
                break

            await asyncio.sleep(0.2)
            i -= 1

        output = thread_result[0]
        if (output is not None):
            break

        tries -= 1
        print("tries left:", tries)

    if (output is None):
        raise Exception("OpenAI timeout!")

    return output

async def get_eleuther_text_direct(query, response_size, temperature, discord_channel, gpt_engine, voice_mode = False, voice_type = None, gen_mode = "raw", repetition_penalty = 0, personality_name = None):
    global last_ai_request_sent
    global aio_session
    global servicing_request
    global textsynth_api_key

    out_audio = None
    msg_audio_transcript = None

    try:
        # https://platform.openai.com/docs/models
        if (gpt_engine in ["gpt-3.5", "gpt-4o-mini", "gpt-4o", "gpt-4.5"]):
            if (discord_channel is not None):
                await discord_channel.trigger_typing()

            error = False

            if (gpt_engine == "gpt-3.5"):
                engine = "gpt-3.5-turbo"
            elif (gpt_engine == "gpt-4o-mini"):
                engine = "gpt-4o-mini"
            elif (gpt_engine == "gpt-4o"):
                engine = "gpt-4o"
            elif (gpt_engine == "gpt-4.5"):
                engine = "gpt-4.5-preview"

            if (voice_mode):
                engine = "gpt-4o-audio-preview"

            try:
                if (gen_mode == "chat"):
                    #print(query)
                    messages = []
                    for entry in query:

                        content = []

                        for c in entry["content"]:
                            if (c["type"] == "text"):
                                print("getting msgs", c)
                                content.append({"type": "text", "text": c["content"]})
                            elif (c["type"] == "audio" and voice_mode):
                                print("getting msgs", c)
                                content.append({"type": "input_audio", "input_audio": {"data": c["content"], "format": "mp3"}})

                        if ("author_name" in entry):
                            messages.append({"role": entry["author_type"], "name": entry["author_name"], "content": content})
                        else:
                            messages.append({"role": entry["author_type"], "content": content})
                        #messages.append({"role": "system", "content": entry[1]})

                    # print("Messages:")
                    # for entry in messages:
                    #     print(entry)

                    out = await get_openai_response(messages, engine, response_size, temperature, repetition_penalty, voice_mode, voice_type)
                    out_text = out["text"]

                    if ("audio" in out):
                        out_audio = out["audio"]

                    if (personality_name is not None):
                        f = personality_name + ":"
                        if (out_text.startswith(f)):
                            out_text = out_text[out_text.find(f) + len(f):]

                    out_text = out_text.strip()

                    if (out_text[0] == '"'):
                        out_text = out_text[1:]

                    if (len(out_text) >= 2):
                        if (out_text[1] == out_text[1].lower()):
                            out_text = out_text[0].lower() + out_text[1:]

                    if (out_text[-1] == '.'):
                        out_text = out_text[:-1]

                    filter_list = ["language model", "cannot generate"]
                    for e in filter_list:
                        if (e in out_text.lower()):
                            error = True
                            out_text = "[OpenAI didnt like that]"
                else:
                    print(query)

                    messages = []
                    messages.append({"role":"system", "content":"Hello! Your task is going to be very simple. You won't be chatting at all, your task is to directly simply continue the text the user sends. That's all! Continue the text in the same exact style as the user, whichever it may be. Don't say anything other than how the user's text should continue"})
                    messages.append({"role":"user", "content": query})

                    out = await get_openai_response(messages, engine, response_size, temperature, repetition_penalty)
                    out_text = out["text"]
                    out_text = " " + out_text.strip()

                    print(out_text)

            # except openai.error.RateLimitError:
            #     traceback.print_exc()
            #     out_text = "[Out of power] - *Consider switching the core's fabric with ^core_set core_fabric*"
            #     error = True

            # except openai.error.Timeout:
            #     traceback.print_exc()
            #     out_text = "[Core timeout]"
            #     error = True

            # except openai.error.InvalidRequestError:
            #     traceback.print_exc()
            #     out_text = "[Invalid core settings]"
            #     error = True

            except:
                traceback.print_exc()
                out_text = "[Unknown Error]"
                error = True

            return {"error": error, "query": query, "output": out_text, "out_audio": out_audio}

        elif (gpt_engine == "chat-gpt"):
            if (discord_channel is not None):
                await discord_channel.trigger_typing()

            #query = "\n".join(query.split("\n")[:-1])
            #query += "\n"
            #print(query)

            #browser.get('https://chat.openai.com/chat')
            textarea = browser.find_element(By.CSS_SELECTOR, 'textarea')
            textarea.send_keys(query)

            btn = browser.find_element(By.CSS_SELECTOR, 'textarea + button')
            btn.click()

            lastmsg = ""
            timeout = 10
            while True:
                msgs = browser.find_elements(By.CSS_SELECTOR, '.text-base')

                msg = msgs[-1].text
                print(len(msg), msg)

                warn = "This content may violate our content policy. If you believe this to be in error, please submit your feedback"

                if warn in msg:
                    msg = msg[:msg.find(warn)]

                #if ('"' in msg):
                #    msg = msg[msg.find('"'):]

                if (msg == lastmsg and len(msg) != 0):
                    if (msg.count('"') >= 2 or True):
                        lastmsg = msg
                        break
                    else:
                        timeout -= 1
                        if (timeout <= 0):
                            lastmsg = msg
                            if (len(lastmsg) == 0):
                                lastmsg = "[Core empty]"
                            break

                lastmsg = msg
                time.sleep(2)

            lastmsg = lastmsg.strip()
            if (lastmsg[0] == '"'):
                lastmsg = lastmsg[1:]

            query = query[:query.find("\n\nInside double quotes, ")] + "\nMalka says \""
            return {"error": False, "query": query, "output": lastmsg}

        elif (gpt_engine == "gpt-3"):
            # if (discord_channel is not None):
            #     await discord_channel.trigger_typing()

            # error = False

            # try:
            #     if (gen_mode == "chat"):
            #         out = openai.Completion.create(
            #             engine="text-davinci-002",
            #             prompt=query,
            #             temperature=temperature,
            #             max_tokens=response_size,
            #             top_p=1,
            #             frequency_penalty=repetition_penalty,
            #             presence_penalty=repetition_penalty,
            #             stop=["\"\n"]
            #         )
            #     else:
            #         out = openai.Completion.create(
            #             engine="text-davinci-002",
            #             prompt=query,
            #             temperature=temperature,
            #             max_tokens=response_size,
            #             top_p=1,
            #             frequency_penalty=repetition_penalty,
            #             presence_penalty=repetition_penalty
            #         )

            #     print(out)
            #     out_text = out['choices'][0]['text']
            # except openai.error.RateLimitError:
            #     traceback.print_exc()
            #     out_text = "[Out of power] - *Consider switching the core's fabric with ^core_set core_fabric*"
            #     error = True

            # except openai.error.InvalidRequestError:
            #     traceback.print_exc()
            #     out_text = "[Invalid core settings]"
            #     error = True

            # except:
            #     traceback.print_exc()
            #     out_text = "[Rate Limit]"
            #     error = True

            # return {"error": error, "query": query, "output": out_text}
            return {"error": True, "query": query, "output": "Unsupported"}

        elif (gpt_engine == "gpt-20b"):
            if (discord_channel is not None):
                await discord_channel.trigger_typing()

            response_size = min(response_size, 200)

            if (textsynth_api_key == None):
                await refresh_textsynth_api_key()

            if (gen_mode == "chat"):
                data = {
                    "prompt": query,
                    "top_p": 1.0,
                    "top_k": 40,
                    "temperature": temperature,
                    "max_tokens": response_size,
                    "stream": False,
                    "stop": '"\n'
                }
            else:
                data = {
                    "prompt": query,
                    "top_p": 1.0,
                    "top_k": 40,
                    "temperature": temperature,
                    "max_tokens": response_size,
                    "stream": False,
                    "stop": None
                }

            print(data)
            i = 0
            max_i = 20

            while (i < max_i):
                ip = [str(random.randint(1, 255)) for i in range(4)]
                print("ip:", ip)

                headers = {
                    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0",
                    "X-Forwarded-For": ".".join(ip),
                    "Authorization": f"Bearer {textsynth_api_key}"
                }

                async with aio_session.post("https://api.textsynth.com/v1/engines/gptneox_20B/completions", headers = headers, json=data, ssl=False) as resp: # ssl=False
                    if (resp.status == 200):
                        resp_json = await resp.json()
                        if ("text" in resp_json):
                            out = resp_json["text"].strip()
                            if (len(out) > 0):
                                return {"error": False, "query": query, "output": out}

                    print("There was an error in the response: ", resp)

                await asyncio.sleep(3)
                await refresh_textsynth_api_key()
                await asyncio.sleep(1)
                i += 1

            return {"error": True, "query": query, "output": "[Core error]"}
        elif (gpt_engine == "gpt-j-forefront"):
            if (discord_channel is not None):
                await discord_channel.trigger_typing()

            if (gen_mode == "chat"):
                data = {
                    "text": query,
                    "top_p": 1.0,
                    "top_k": 40,
                    "temperature": temperature,
                    "length": response_size,
                    "repetition_penalty": repetition_penalty,
                    "stop": ['"\\n'],
                    "bad_words": [],
                    "logit_bias": []
                }
            else:
                data = {
                    "text": query,
                    "top_p": 1.0,
                    "top_k": 40,
                    "temperature": temperature,
                    "length": response_size,
                    "repetition_penalty": repetition_penalty,
                    "stop": [],
                    "bad_words": [],
                    "logit_bias": []
                }

            print(data)
            i = 0
            max_i = 20

            while (i < max_i):
                async with aio_session.post("https://playground-api.forefront.link/api/models/gpt-j", json=data, ssl=False) as resp: # ssl=False
                    if (resp.status == 200):
                        resp_json = await resp.json()
                        if ("result" in resp_json and len(resp_json["result"]) > 0 and "completion" in resp_json["result"][0]):
                            out = resp_json["result"][0]["completion"].strip()
                            if (len(out) > 0):
                                if (gen_mode == "chat"):
                                    if ('"\n' in out):
                                        out = out[:out.find('"\n')]
                                        print("Had to manually remove the stop symbol...")

                                return {"error": False, "query": query, "output": out}

                    print("There was an error in the response: ", resp)

                await asyncio.sleep(2)
                i += 1

            return {"error": True, "query": query, "output": "[Core error]"}

        elif (gpt_engine == "gpt-j-eleuther"):
            headers = {
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0",
                'Accept': 'application/json',
                'Referer': 'https://6b.eleuther.ai/',
                'Origin': 'https://6b.eleuther.ai'
            }

            data = {
                "context": query,
                "top_p": 1,
                "temp": temperature,
                "response_length": response_size,
                "remove_input": True
            }

            #print("Activate proxy now!!")
            #await asyncio.sleep(2)

            i = 0
            max_i = 20
            error500s = 0

            print("requesting")
            while (i < max_i):
                sleep_time = request_delay - (time.time() - last_ai_request_sent)

                if (sleep_time > 0):
                    await asyncio.sleep(sleep_time)

                last_ai_request_sent = time.time()

                while (servicing_request):
                    await asyncio.sleep(0.1)

                servicing_request = True
                try:
                    quit_typing = False

                    async def typing_event():
                        await asyncio.sleep(1.5 + random.random() * 2)
                        max_i2 = 10
                        i2 = 0
                        while (not quit_typing) and i2 < max_i2:
                            #print("typing", quit_typing)
                            #print("trigger typing")
                            await discord_channel.trigger_typing()
                            #print("trigger typing done")
                            await asyncio.sleep(9)
                            i2 += 1

                        #print("typing end ", quit_typing)

                    if (discord_channel is not None):
                        asyncio.create_task(typing_event())

                    async with aio_session.post("https://api.eleuther.ai/completion", headers = headers, json=data) as resp:
                        servicing_request = False
                        print(resp)
                        if (resp.status == 200):
                            resp_json = await resp.json()
                            if (isinstance(resp_json, list) and len(resp_json) >= 1 and "generated_text" in resp_json[0]):
                                out = resp_json[0]["generated_text"]

                                quit_typing = True
                                if (len(out) == 0):
                                    return {"error": True, "query": query, "output": "[Core empty]"}
                                else:
                                    if (gen_mode == "chat"):
                                        out = out.strip()
                                        if ('"\n' in out):
                                            out = out[:out.find('"\n')]

                                    return {"error": False, "query": query, "output": out}
                            else:
                                quit_typing = True
                                return {"error": True, "query": query, "output": "[Core returned nonsense]"}

                        else:
                            quit_typing = True
                            print(f"Got {resp.status}. Retrying in: {sleep_time}. Try: {i}/{max_i}")

                            if (resp.status != 503):
                                i += 1

                                if (resp.status == 500):
                                    query = query[len(query) // 4:]

                                    if (query.find("\n") == -1):
                                        query = ""
                                    else:
                                        query = query[query.find("\n") + 1:]

                                    if (len(query) == 0):
                                        return {"error": True, "query": query, "output": "[Core dead]"}

                                    data["context"] = query

                                    print("Truncating chat history...")
                                    print("DATA CONTEXT", data["context"])

                except:
                    print(traceback.print_exc())
                    i += 1

                servicing_request = False

            return {"error": True, "query": query, "output": "[Core timeout]"}

        elif (gpt_engine in ["claude-3-5-sonnet", "claude-3-7-sonnet"]):
            if (discord_channel is not None):
                await discord_channel.trigger_typing()

            if (gen_mode == "raw"):
                print(query)

                out_text = anthropic_client.messages.create(
                    model=gpt_engine+"-latest",
                    max_tokens=response_size,
                    temperature=temperature,
                    system="Hello! Your task is going to be very simple. You won't be chatting at all, your task is to directly simply continue the text the user sends. That's all! Don't say anything other than how the user's text should continue",
                    messages = [{"role": "user", "content": query}]
                )

                out_text = out_text.content[0].text.strip()

                print(out_text)

            else:
                messages = []
                msg = ""

                system_prompt = ""

                for entry in query:
                    content = []

                    if (entry["author_type"] == "system"):
                        for c in entry["content"]:
                            if (c["type"] == "text"):
                                system_prompt += c["content"] + "\n"
                        continue

                    for c in entry["content"]:
                        if (c["type"] == "text"):
                            if ("author_name" in entry):
                                content.append({"type": "text", "text": entry["author_name"] + ": " + c["content"]})
                            else:
                                content.append({"type": "text", "text": c["content"]})
                        else:
                            if ("author_name" in entry):
                                if ("transcript" in c):
                                    content.append({"type": "text", "text": entry["author_name"] + ": " + c["transcript"]})
                                else:
                                    content.append({"type": "text", "text": entry["author_name"] + " sent an audio file"})
                            else:
                                if ("transcript" in c):
                                    content.append({"type": "text", "text": c["transcript"]})
                                else:
                                    content.append({"type": "text", "text": "an audio file was sent"})

                    # if (len(content) == 0):
                    #     content.append({"type": "text", "text": ""})

                    messages.append({"role": entry["author_type"], "content": content})
                    #messages.append({"role": "system", "content": entry[1]})

                # for entry in query:
                #     if (entry[0] != "system"):
                #         messages.append({"role": entry[0], "content": entry[1]})
                #         msg += entry[1] + "\n"

                if (len(messages) > 0 and messages[0]["role"] == "assistant"):
                    messages = messages[1:]

                messages.append({"role": "assistant", "content": [{"type": "text", "text": f"Ok yeah! I see the convo above, I can totally continue it as {personality_name}. So I'd respond with something like: \""}]})

                print(messages)

                #messages.append({"role": "user", "content": msg})

                # print("Messages:")
                # for entry in messages:
                #     print(entry)

                # out_text = random.choice(["\* crank *", "\* slam *", "\* shing *"])

                out_text = anthropic_client.messages.create(
                    model=gpt_engine+"-latest",
                    max_tokens=response_size,
                    temperature=temperature,
                    #system="Hi! You are Malka! a 20 year old young woman who REALY loves CUTE things! You are currently chatting on Discord with a few of your friends who you've been talking to for a really long time. They are used to you being super silly and wacky in the past since you used to say a lot of nonsensical stuff, but now you're trying to kind of get a better grasp of reality and be more present in this world! Still, your writing style is still pretty sweet and super gen-z like, u love using a TON of abbreviations n stuff like that, u like using some emojis here and there and also swear a bunch like shit, fuck etc. U don't really like sending messages that are long and prefer to chat short n cool! Talk just like anyone else on Discord btw, don't stand out with super long msgs. Anyway, got it? Time to get into the chatttt :D",

                    system = system_prompt,
                    messages = messages
                )

                out_text = out_text.content[0].text.strip()

                if (personality_name is not None):
                    f = personality_name
                    if (out_text.startswith(f)):
                        out_text = out_text[out_text.find(f) + len(f):]

                out_text = out_text.strip()

                if (out_text[0] == ':'):
                    out_text = out_text[1:].strip()

                if (out_text[0] == '"'):
                    out_text = out_text[1:]

                if (len(out_text) >= 2):
                    if (out_text[1] == out_text[1].lower()):
                        out_text = out_text[0].lower() + out_text[1:]

                if (out_text[-1] == '.'):
                    out_text = out_text[:-1]

                print(out_text)

                if (voice_mode):
                    messages = []

                    for entry in query:
                        content = []

                        for c in entry["content"]:
                            if (c["type"] == "text"):
                                content.append({"type": "text", "text": c["content"]})
                            elif (c["type"] == "audio"):
                                content.append({"type": "input_audio", "input_audio": {"data": c["content"], "format": "mp3"}})

                        if ("author_name" in entry):
                            messages.append({"role": entry["author_type"], "name": entry["author_name"], "content": content})
                        else:
                            messages.append({"role": entry["author_type"], "content": content})

                    messages.append({"role": "system", "content":[{"type":"text", "text":f"Ok! so you've kinda thought of something to say, it's: \"{out_text}\". Remember it's SUPER important to talk exactly like how your character would talk! Loads of expressiveness (or complete lack of, if it's in character) is super super important, ur friends will absolutely love it!"}]})

                    out = await get_openai_response(messages, "gpt-4o-audio-preview", response_size, temperature, repetition_penalty, voice_mode, voice_type)

                    if ("audio" in out):
                        out_audio = out["audio"]
                        msg_audio_transcript = out["text"]

                    print("audio_transcript", msg_audio_transcript)

            return {"error": False, "query": query, "output": out_text, "out_audio": out_audio, "audio_transcript": msg_audio_transcript}

    except Exception:
        servicing_request = False
        timeout = aiohttp.ClientTimeout(total=60 * 20)
        aio_session = aiohttp.ClientSession(timeout = timeout)
        traceback.print_exc()
        return {"error": True, "query": query, "output": "[Core dead x2]"}

async def conjuration(message, query, params, replied_msg):
    url = "4.tcp.ngrok.io:19067"

    original_query = query

    if (message.attachments is not None and len(message.attachments) >= 1):
        query = "IMAGE " + message.attachments[0].proxy_url
    else:
        query = "TEXT " + query

    try:
        reader, writer = await asyncio.open_connection(url.split(":")[0], url.split(":")[1])

        for p in params:
            query += f"--{p}:{params[p]} "

        writer.write(query.encode())
        await writer.drain()

        data = await reader.read()

        print("received total:", len(data))

        writer.close()
        await writer.wait_closed()

        data = data.decode().split("\n")

        gif = data[0]
        ply = data[1]
        obj = data[2]

        print("received", len(gif), len(ply), len(obj))

        fname = getPath() + f"/tmp/{message.id}"

        fp = open(fname + ".gif", "wb")
        fp.write(base64.b64decode(gif))
        fp.close()

        fp = open(fname + ".ply", "wb")
        fp.write(base64.b64decode(ply))
        fp.close()

        fp = open(fname + ".obj", "wb")
        fp.write(base64.b64decode(obj))
        fp.close()

        files = []
        fps = []

        exts = [".gif"]

        if ("--get-objs" in query):
            exts.append(".ply")
            exts.append(".obj")

        for e in exts:
            fp = open(fname + e, "rb")
            fps.append(fp)
            files.append(disnake.File(fp, filename = "conjuration" + e))

        msgs = ["appeared!", "spawned in front of you!", "has been summoned", "has appeared"]
        await message.reply(f"**{original_query}** {random.choice(msgs)}", files = files)

        for fp in fps:
            fp.close()

        for e in exts:
            os.remove(fname + e)

        try:
            await replied_msg.delete()
        except:
            traceback.print_exc()

    except:
        traceback.print_exc()
        return ""

blank_core = Core("malka", default_gpt_engine, None)

async def dril_gen():
    global blank_core

    query = """^Best tweets by Dril, best @dril Twitter posts

1. another day volunteering at the betsy ross museum, everyone keeps asking me if they can fuck the flag, buddy, they wont even let me fuck it
2. if your grave doesnt say "rest in peace" on it you are automatically drafted into the skeleton war
3. "im not owned! Im not owned!", i continue to insist as i slowly shrink and transform into a corn cob
4. "This Whole Thing Smacks Of Gender", i holler as i overturn my uncle's barbeque grill and turn the 4th of July into the 4th of Shit
5."""

    msg = ""

    ai_return_obj = await get_eleuther_text(query, blank_core)
    if (not ai_return_obj["error"]):
        out = ai_return_obj["output"].strip().split("\n")[0].strip()

        if (out[0] == '"' and out[-1] == '"'):
            out = out[1:-1]

        msg = "> " + out

    return msg

@bot.event
async def on_ready():
    global aio_session

    print(bot.user.name + " powered up!")
    await bot.change_presence(activity = disnake.Game(name = f"^core_help | vvvvVVVVVVVVVVVVVVVVVV * continuous high-pitched screech *"))
    aio_session = aiohttp.ClientSession()

@bot.event
async def on_message(message):
    global cores
    global pending_personality_core
    global personality_core_confirmations
    global stupid_objs
    global sleeping
    global aklam_coins
    global core_gen_voice_next
    global core_gen_voice_next_voice

    if (message.guild is None): # disable DMs
        return

    if message.author == bot.user:
        return

    raw_inp = message.content
    uid = message.author.id
    ch_id = message.channel.id

    if (not raw_inp.startswith(prefix)):
        if (ch_id in cores and cores[ch_id].voice_mode):
            if (not (len(message.attachments) > 0 and "audio/ogg" in message.attachments[0].content_type)):
                return
        else:
            return

    # setup allowed personality cores
    core_templates = personality_core_templates.copy()

    default_core = "gnarp_gnap"

    if (message.guild.id in [760098399869206529]):
        for c in list(core_templates.keys()):
            if ("_dangerous" in c):
                del core_templates[c]
    else:
        default_core = "nagito"

    # default_core = "malka_claude"

    if (len(raw_inp) >= len(prefix)):
        raw_inp = raw_inp[len(prefix):]
        inp = raw_inp.lower().split()
    else: # voice message
        raw_inp = ""
        inp = [""]

    msg = ""
    embed = None
    response_audio = None

    if ch_id not in cores:
        cores[ch_id] = Core(default_core, default_gpt_engine, message.channel)

    if (inp[0] == "core_probe"):
        if (sleeping):
            msg = f":low_brightness:  **PROBE RESPONSE: `[{core_name}]` **OFF"
        else:
            msg = f":sunny:  **PROBE RESPONSE: `[{core_name}]` ON**"

        await message.channel.send(msg)
        return

    if (inp[0] == "core_down"):
        if ((not sleeping) and (inp[1].lower() == core_name.lower())):
            sleeping = True
            msg = f":arrow_down:  **`[{core_name}]` CORE DOWN.**"
            await message.reply(msg)

        return

    if (inp[0] == "core_up"):
        if (sleeping and (inp[1].lower() == core_name.lower())):
            sleeping = False
            msg = f":arrow_up:  **`[{core_name}]` CORE UP.**"
            await message.reply(msg)

        return

    if (sleeping):
        return

    shadowban = [238755156290699264]
    chatted = False

    if (inp[0] == "core_help"):
        cmds = {
            "core_help": "Open help menu",
            "core_status": "List core information",
            "core_reset": "Clear a conversation's internal chat history",
            "core_set": "Set a core variable",
            "core_gen": f"Continue text using {cores[ch_id].gpt_engine.upper()}",
            "core_gen_voice": f"Generate a voice sample from text or an attached audio file!",
            "core_gen_voice_next": f"Convert the next audio file / recording you send into the voice you supply!",
            "[any text]": "Talk with the personality core assigned to the channel"
        }

        msg = f"{help_header}\n\n:information_source:  **HELP**\n"

        for cmd in cmds:
            msg += f"` {prefix}{cmd.ljust(11, ' ')} `  **{cmds[cmd]}**\n"

    elif (inp[0] == "core_status"):
        msg = f"{help_header}\n\n:low_brightness: **<#{ch_id}> CORE STATUS**\n"

        settings = cores[ch_id].get_settings()
        fmt_normal = "` {} `  **{}**\n"
        fmt_modes = "` {} `  **{} CHAT  /  {} RAW**\n"

        msg += fmt_normal.format("personality_core    ", cores[ch_id].personality_core_id.upper())
        msg +=  fmt_modes.format("temperature         ", settings["chat"]["temperature"], settings["raw"]["temperature"])
        msg +=  fmt_modes.format("output_size         ", settings["chat"]["output_size"], settings["raw"]["output_size"])
        msg +=  fmt_modes.format("repetition_penalty  ", settings["chat"]["repetition_penalty"], settings["raw"]["repetition_penalty"])
        msg += fmt_normal.format("chat_length         ", settings["chat"]["chat_length"])
        msg += fmt_normal.format("voice_mode          ", str(cores[ch_id].voice_mode).upper())
        msg += fmt_normal.format("voice_type          ", str(cores[ch_id].voice_type).upper())
        msg += fmt_normal.format("gen_voice_use_openai", str(cores[ch_id].gen_voice_use_openai).upper())
        msg += fmt_normal.format("core_fabric         ", cores[ch_id].gpt_engine.upper())

    elif (inp[0] == "core_reset"):
        #browser.get('https://chat.openai.com/chat')
        cores[ch_id].reset()
        msg = "**CORE RESET OK.**"

    elif (inp[0] == "core_confirm"):
        if (pending_personality_core is not None):
            timeout = int(time.time()) - 10
            for u in personality_core_confirmations:
                if (personality_core_confirmations[u] < timeout):
                    del personality_core_confirmations[u]

            personality_core_confirmations[uid] = int(time.time())

            limit = 2
            if (len(personality_core_confirmations) < limit):
                msg = f"**:arrows_counterclockwise: CORESWAP CONFIRMATION: {len(personality_core_confirmations)}/{limit} (in the past 10 seconds)**"
            else:
                msg = f"**:warning: CORE ENABLED: {pending_personality_core}**"
                cores[ch_id].set_personality_core(pending_personality_core)
                pending_personality_core = None
                personality_core_confirmations = {}

        else:
            msg = "There is no core swap needing confirmation."

    elif (inp[0] == "core_set"):
        settings = cores[ch_id].get_settings()
        core_fabrics = list(cores[ch_id].settings)

        msg = f"Usage: {prefix}core_set temperature_raw 0.9\n\n"
        msg += "Variables available:\n"
        msg += f"- personality_core: **{cores[ch_id].personality_core_id}**\n"
        msg += f"(Available personality cores: {list(core_templates.keys())})\n\n"
        msg += f"- core_fabric: **{cores[ch_id].gpt_engine}**\n"
        msg += f"(Available core fabrics: {core_fabrics})\n\n"

        for mode in settings:
            for var, value in settings[mode].items():
                msg += f"- {var}_{mode}: **{value}**\n"
            msg += "\n"

        if (len(inp) == 3):
            var = inp[1]
            input_val = inp[2]

            print("inp", var, input_val, var == "personality_core")

            if (var == "personality_core"):
                if (input_val in core_templates):
                    if ("dangerous" in input_val and False): # "and False" = disable _dangerous restriction
                        pending_personality_core = input_val
                        personality_core_confirmations = {}
                        msg = ":warning: **WARNING!** The core you are trying to enable is marked as **dangerous**. Type `^core_confirm` to confirm selection."
                    else:
                        msg = f"Personality core set to **{input_val}**!"
                        cores[ch_id].set_personality_core(input_val)
                else:
                    msg = "Invalid personality core. Available cores: " + str(list(core_templates.keys()))
            elif (var == "core_fabric"):
                input_val = input_val.lower()

                if (input_val in core_fabrics):
                    msg = f"Core fabric set to **{input_val}**!"
                    cores[ch_id].gpt_engine = input_val
                else:
                    msg = f"Invalid core fabric. Available fabrics: {core_fabrics}"
            elif (var == "voice_mode"):
                input_val = input_val.lower()

                if (input_val in ["true", "false"]):
                    cores[ch_id].voice_mode = (input_val == "true")
                    msg = f"Voice mode set to **{cores[ch_id].voice_mode}**!"
                else:
                    msg = f"Invalid voice mode. Only true and false are allowed!"
            elif (var == "gen_voice_use_openai"):
                input_val = input_val.lower()

                if (input_val in ["true", "false"]):
                    cores[ch_id].gen_voice_use_openai = (input_val == "true")
                    msg = f"gen_voice_use_openai set to **{cores[ch_id].gen_voice_use_openai}**!"
                else:
                    msg = f"Invalid gen_voice_use_openai. Only true and false are allowed!"
            elif (var == "voice_type"):
                input_val = input_val.lower()
                allowed_values = ["ash", "ballad", "coral", "sage", "verse", "alloy", "echo", "shimmer"]

                if (input_val in allowed_values):
                    cores[ch_id].voice_type = input_val
                    msg = f"Voice type set to **{cores[ch_id].voice_type}**!"
                else:
                    msg = f"Invalid voice type. Available voice types: {allowed_values}"
            else:
                mode = var.split("_")[-1]
                var = var[:var.rfind("_")]
                print(mode, var, input_val)

                if (mode in settings and var in settings[mode]):
                    value = settings[mode][var]
                    try:
                        value = float(input_val)
                        if (math.isnan(value)):
                            value = settings[mode][var]
                    except: traceback.print_exc()

                    settings[mode][var] = value

                    msg = f"Variable **{var}_{mode}** is now set to: **{settings[mode][var]}**"

    elif (inp[0] == "core_gen_reset"):
        cores[ch_id].reset_pre_gen(uid)
        msg = "**OK.**"

    elif (inp[0] == "core_gen_build"):
        query = raw_inp[len(inp[0]) + 1:]
        cores[ch_id].append_pre_gen(uid, query + " ")

        text = cores[ch_id].get_pre_gen(uid)

        msg = f"**OK.** Pre-message length is currently **{len(text)}** chars:\n>>> {text[:100]} [...] {text[-100:]}"

    elif (inp[0] == "core_gen"):
        query = cores[ch_id].get_pre_gen(uid)
        query += raw_inp[len(inp[0]) + 1:]
        query_bak = query

        if (uid in shadowban):
            query = random.choice(["Here are top 5 recipes to try when you are getting BORED out of your mind!", "3 Extreme date ideas:", "So me and my dog went out for a walk, when", "TOTAL EXPLOSION! TOTAL DEVASTIATION! You are never going to"])

        ai_return_obj = await get_eleuther_text(query, cores[ch_id])

        if (not ai_return_obj["error"]):
            if (uid in shadowban):
                msg = query_bak + "**" + " " + ai_return_obj["output"]
            else:
                msg = ai_return_obj["query"] + "**" + " " + ai_return_obj["output"]

            msg = msg[-2000:]
            if (len(msg) == 2000):
                msg = "..." + msg[5:]

            msg = "**" + msg
        else:
            msg = ai_return_obj["output"]

    elif (inp[0] == "core_edit"):
        query = raw_inp[len(inp[0]) + 1:]

        if (len(query) > 0):
            if (message.reference is not None):
                ref_id = message.reference.message_id
            else:
                ref_id = cores[ch_id].chat_get_last_msg()["id"]

            ref = await cores[ch_id].discord_channel.fetch_message(ref_id)
            ok = cores[ch_id].chat_edit_msg(ref.id, query)

            if (ok):
                await ref.edit(query)
                msg = f"**OK.** Core reponse updated. Old message was:\n```{ref.content}```"
            else:
                msg = "Fail."

        else:
            msg = "You can't replace it with an empty message!"

        ok = False

    elif (inp[0] == "core_undo"):
        query = raw_inp[len(inp[0]) + 1:]

        try:
            value = int(query)
            if (value > 0):
                cores[ch_id].chat_undo(value)
                m = cores[ch_id].chat_get_last_msg()
                msg = f"**OK.** The last chat message is now by {m['author_name']}: \"{m['content']}\""
            else:
                msg = "No funny business."
        except:
            msg = "You can only undo an integer amount of messages"

    # elif (inp[0] == "core_reroll"):
    #     m = cores[ch_id].chat_get_last_msg()
    #     editable_msg = await cores[ch_id].discord_channel.fetch_message(m["id"])



    elif (inp[0] == "tweet"):
        instrs = ""

        if (len(inp) > 1):
            u = raw_inp[len(inp[0]) + 1:]
            if (" " in u):
                instrs = " " + " ".join(u.split(" ")[1:])
                u = u.split(" ")[0]
        else:
            u = "Leahx"

        original_u = u

        if (uid in shadowban):
            u = "Leahx"
            instrs = ""

#         query = f"""^Best tweets by {u}, best @{u} Twitter posts{instrs}

# 1. another day volunteering at the betsy ross museum, everyone keeps asking me if they can fuck the flag, buddy, they wont even let me fuck it
# 2. if your grave doesnt say "rest in peace" on it you are automatically drafted into the skeleton war
# 3. "im not owned! Im not owned!", i continue to insist as i slowly shrink and transform into a corn cob
# 4. "This Whole Thing Smacks Of Gender", i holler as i overturn my uncle's barbeque grill and turn the 4th of July into the 4th of Shit
# 5."""

        query = f"""^Best tweets by {u}, best @{u} Twitter posts{instrs}

1. customer: is there more wolves and less meat here?
me with more meat and less wolves gun:
2. smoking straight grave moss man im smoking on that shit that grows on the bones of heroes long-forgotten
3. if your grave doesnt say "rest in peace" on it you are automatically drafted into the skeleton war
4. "im not owned! Im not owned!", i continue to insist as i slowly shrink and transform into a corn cob
5. "This Whole Thing Smacks Of Fish", i holler as i overturn my uncle's barbeque grill and turn the 4th of July into the 4th of Shit
6."""

        ai_return_obj = await get_eleuther_text(query, cores[ch_id])
        if (not ai_return_obj["error"]):
            out = ai_return_obj["output"].strip().split("\n")
            msg = ">>> "

            ok = False

            for tweet in out:
                if ('wise man bowed his head solemnly and spoke' in tweet and 'theres actually zero difference between good' in tweet and 'you imbecile. you fucking moron' in tweet):
                    continue

                if (len(tweet) > 0):
                    if (". " in tweet and tweet[:tweet.find(". ")].isnumeric()):
                        tweet = tweet[tweet.find(". ") + 2:].strip()
                        msg += "- "
                    else:
                        if (msg == ">>> "):
                            msg += "- "
                        else:
                            msg += "  "

                    if (tweet[0] == '"' and tweet[-1] == '"'):
                        tweet = tweet[1:-1]

                    msg += tweet + "\n"

                    ok = True

            if (original_u != "Leahx"):
                msg += f"\n@{original_u}"

            if (not ok):
                msg = "[The tweet box came out empty, only meaningless dust can be found inside]"
        else:
            msg = ai_return_obj["output"]

    elif (inp[0] == "conjure"):
        query = raw_inp[len(inp[0]) + 1:]
        params = {}

        msg_error = ""

        if ("--" in query):
            params_txt = query[query.find("--"):]
            query = query[:query.find("--")].strip()

            ps = params_txt.split("--")
            for p in ps:
                try:
                    p = p.strip()
                    if (" " not in p):
                        if (len(p) > 0):
                            params[p] = "true"
                        continue

                    param = p.split(" ")[0]
                    value = p.split(" ")[-1]

                    if (param == "size"):
                        values = [32, 64, 128, 256, 512]
                        if (int(value) not in values):
                            msg_error = f"Value for {param} must be in: {values}"
                        else:
                            params[param] = value
                    elif (param == "guidance_scale"):
                        params[param] = float(value)
                    else:
                        msg_error = f"Param must be in: {['size']}"
                except:
                    traceback.print_exc()

        if (len(msg_error) != 0):
            msg = msg_error
        else:
            if (len(query) == 0):
                query = "thing"

            replied_msg = await message.reply(f":comet: CONJURATION OF **{query}** STARTED.")

            obj = conjuration(message, query, params, replied_msg)
            stupid_objs.append(obj)
            await obj

    elif (inp[0] == "aklam_coins"):
        await message.reply(f":loudspeaker: :anger_right: You have **{aklam_coins} Aklam coins** :coin: left")

    elif (inp[0] == "aklam_coins_add"):
        if (uid != 184564860804136960):
            await message.reply(":loudspeaker: :anger_right: You cannot add **Aklam coins**")
        else:
            amount = int(inp[1])
            if (amount > 0):
                aklam_coins += amount
                await message.reply(f":loudspeaker: :anger_right: Aklam coins added: **+{amount} :coin:**")
            else:
                aklam_coins += amount
                await message.reply(f":loudspeaker: :anger_right: Aklam coins subtracted: **{amount} :coin:**")
                if (aklam_coins < 0):
                    aklam_coins = 0

    elif (inp[0] == "envision"):
        paid = True
        fname = f"tmp/{message.id}.png"

        if (paid and aklam_coins <= 0):
            await message.reply(":loudspeaker: :anger_right: You have **0 Aklam coins** left")
        else:
            # engine is [sd3, dalle, a1111]
            params = {"width": 512, "height": 384, "ar": "1:1", "steps": 30, "lora_detail": 0, "follow_prompt": 7, "seed": -1, "subseed": -1, "follow_image": 0.075, "engine": "sd3"} # old cfg = 14
            query = raw_inp[len(inp[0]) + 1:]

            if (uid in shadowban):
                query = random.choice(["extreme beach location at a resort", "insane mobile phone", "super tiktok addiction thing"])

            negatives = ""
            msg_error = ""

            if (params["engine"] == "a1111"):
                try:
                    api_url = "http://127.0.0.1:7860"
                    r = requests.head(api_url)
                except:
                    msg_response = await message.reply(":last_quarter_moon_with_face: Envisioning is momentarily offline.")
                    return

            if ("--" in query):
                params_txt = query[query.find("--"):]
                query = query[:query.find("--")].strip()

                ps = params_txt.split("--")
                ok_subp = ["nollm", "dalle"]
                ok_p = ["neg"] + list(params.keys()) + ok_subp

                for p in ps:
                    try:
                        p = p.strip()
                        if (" " not in p):
                            if (len(p) > 0):
                                if (p not in ok_subp):
                                    msg_error = f"Params with no argument must be in: {ok_subp}"

                                params[p] = "true"
                            continue

                        param = p.split(" ")[0]
                        value = p.split(" ")[-1]

                        if (param == "neg"):
                            if (len(negatives) > 0):
                                negatives += ", "

                            negatives += " ".join(p.split(" ")[1:]).strip()

                        elif (param == "width"):
                            if (int(value) > 768 or int(value) < 64):
                                msg_error = f"Value for {param} must be within 64 and 768"
                            else:
                                params[param] = value
                        elif (param == "height"):
                            if (int(value) > 768 or int(value) < 64):
                                msg_error = f"Value for {param} must be within 64 and 768"
                            else:
                                params[param] = value
                        elif (param == "steps"):
                            if (int(value) > 50 or int(value) < 0):
                                msg_error = f"Value for {param} must be within 0 and 50"
                            else:
                                params[param] = value
                        elif (param == "lora_detail"):
                            if (float(value) > 10 or float(value) < -10):
                                msg_error = f"Value for {param} must be within -10 and 10"
                            else:
                                params[param] = value
                        elif (param == "seed" or param == "subseed"):
                            params[param] = int(value)
                        elif (param == "follow_prompt"):
                            if (int(value) > 30 or int(value) < 1):
                                msg_error = f"Value for {param} must be within 1 and 30"
                            else:
                                params[param] = value
                        elif (param == "follow_image"):
                            if (len(message.attachments) == 0):
                                msg_error = "--follow_image is present but no image has been given!"
                            elif (float(value) < 0 or float(value) > 1):
                                msg_error = f"Value for {param} must be within 0 and 1"
                            else:
                                params[param] = float(value)
                        elif (param == "ar"):
                            ars = ["16:9", "1:1", "21:9", "2:3", "3:2", "4:5", "5:4", "9:16", "9:21"]
                            value = value.strip()

                            if (value not in ars):
                                msg_error = f"Value for {param} must be in {ars}"
                            else:
                                params[param] = value
                        else:
                            msg_error = f"Param must be in: {ok_p}"
                    except:
                        traceback.print_exc()

            if (len(msg_error) > 0):
                await message.reply(msg_error)
                return

            engine = params["engine"]
            if ("dalle" in params and params["dalle"]):
                engine = "dalle"

            img_to_img = False

            if (len(message.attachments) > 0):
                if (uid not in shadowban):
                    img_to_img = True
                    img_data = requests.get(message.attachments[0].url).content

            if (params["seed"] == -1):
                params["seed"] = random.randint(1, 1000000000)

            if (paid):
                pre_msg = ":money_with_wings: "
            else:
                pre_msg = ""

            if (len(negatives) > 0):
                msg_response = await message.reply(pre_msg + ":eye: Envisioning...... " + query + f" (Negative: {negatives})")
            else:
                msg_response = await message.reply(pre_msg + ":eye: Envisioning...... " + query)

            if (not ("nollm" in params and params["nollm"])):
                if (len(negatives) > 0):
                    query += f" (I also don't want to see any: {negatives})"

                llm_query = f"Hi! So your good friend Malka needs some help with her AI image generator prompt! What u need to do is to understand exactly what Malka wants to make, and describe it in a way that an AI image generator would best understand! If the prompt is really short, make sure to add a LOT of FASCINATING, extreme crazy details and go absolutely hog wild on it! Keep exactly what Malka still wants to see and EXPLODE it with artistic creativity !!!!! AI image generators work best on succint, hyper precise prompts where each word has its precise meaning and there's no waffling about. They also LOVE impactful keywords hyper-describing the medium, ambience and effects of the artwork. Don't shy off appending keywords that mention insane details like camera, artwork medium, cinematographic techniques, impactful colour details, scenic effects, mood, etc!!! Just be creative with it, go WIIIIILD and keep it short, extremely tiny, incredibly artistic and hyper PRECISE. Got it? Good luck!!! (btw in ur response just send your prompt and NOTHING else!!!) Here's the prompt: \"{query}\""

                gpt_engine_bak = cores[ch_id].gpt_engine
                cores[ch_id].gpt_engine = "gpt-4o"
                cores[ch_id].voice_mode = False
                ai_return_obj = await get_eleuther_text(llm_query, cores[ch_id])
                query = ai_return_obj["output"]

                cores[ch_id].gpt_engine = gpt_engine_bak

                if (paid):
                    await msg_response.edit(f"Your prompt has been changed by Malka's Box of Envisioning Tools :mushroom: to **{query}**\n\n:loudspeaker: :anger_right: You have **{aklam_coins - 1} Aklam coins** :coin: left")
                else:
                    await msg_response.edit(f"Your prompt has been changed by Malka's Box of Envisioning Tools :mushroom: to **{query}**")

            total_tries = 5
            tries = 0

            while tries < total_tries:
                try:
                    if (engine == "sd3"):
                        print("query:", query)
                        print("negatives:", negatives)

                        if (img_to_img):
                            response = requests.post(
                                f"https://api.stability.ai/v2beta/stable-image/generate/sd3",
                                headers={
                                    "authorization": "Bearer " + stability_token,
                                    "accept": "image/*"
                                },
                                files={"image": img_data},
                                data={
                                    "prompt": query,
                                    "negative_prompt": negatives,
                                    "seed": params["seed"],
                                    "output_format": "jpeg",
                                    "model" : "sd3.5-large",
                                    "mode": "image-to-image",
                                    "strength": 1 - params["follow_image"]
                                },
                            )

                        else:
                            response = requests.post(
                                f"https://api.stability.ai/v2beta/stable-image/generate/sd3",
                                headers={
                                    "authorization": "Bearer " + stability_token,
                                    "accept": "image/*"
                                },
                                files={"none": ''},
                                data={
                                    "prompt": query,
                                    "negative_prompt": negatives,
                                    "aspect_ratio": params["ar"],
                                    "seed": params["seed"],
                                    "model" : "sd3.5-large",
                                    "output_format": "jpeg",
                                },
                            )

                        if response.status_code == 200:
                            break
                        else:
                            tries += 1
                            print("Error:", response.json())

                            try:
                                if (response.json()["name"] == "content_moderation"):
                                    tries = total_tries
                                    break
                            except:
                                traceback.print_exc()

                    elif (engine == "a1111"):
                        url = "http://127.0.0.1:7860/sdapi/v1/txt2img"
                        prompt = query
                        negative_prompt = negatives

                        data = {
                            "prompt": prompt,
                            "negative_prompt": negative_prompt,
                            "sampler_name": "DPM++ SDE Karras",
                            "steps": params["steps"],
                            "cfg_scale": params["follow_prompt"],
                            "width": params["width"],
                            "height": params["height"],
                            "seed": params["seed"],
                            "subseed": params["subseed"],
                            "save_images": True
                        }

                        files = []
                        async with aio_session.post(url, json=data) as resp: # ssl=False
                            if (resp.status == 200):
                                resp_json = await resp.json()

                                info = json.loads(resp_json["info"])
                                #print(info)
                                img = resp_json["images"][0]
                                discord_name = f"{query}_seed_{info['seed']}_subseed_{info['subseed']}.png"

                                with open(fname, "wb") as fp:
                                    fp.write(base64.b64decode(img))

                                with open(fname, "rb") as fp:
                                    files.append(disnake.File(fp, filename = discord_name))

                        try:
                            await msg_response.delete()
                        except:
                            traceback.print_exc()

                        await message.reply(files = files)
                        break
                    elif (engine == "dalle"):
                        response = client.images.generate(
                            model = "dall-e-3",
                            prompt = query,
                            size = "1024x1024",
                            quality = "standard",
                            n=1
                        )

                        break
                except openai.BadRequestError:
                    traceback.print_exc()
                    tries += 1
                    await msg_response.edit(f":warning: There probably was an OpenAI content policy violation :( I'll ask them nicely again for you! :)  (**{tries} / {total_tries}**)")

                except:
                    traceback.print_exc()
                    aklam_coins -= 1
                    await msg_response.edit("An error happened :( Maybe OpenAI servers are overloaded?")
                    return

            if (tries >= total_tries):
                try:
                    response = response.json()
                    if (response["name"] == "content_moderation"):
                        await message.reply(f":warning: There was a content policy violation, no point in retrying! (unless you used --llm)")
                    else:
                        await message.reply(f":warning: There probably was a content policy violation (or we ran out of the real Aklam Coins :coin:) Feel free to retry!")

                except:
                    tracback.print_exc()
                    await message.reply(f":warning: There probably was a content policy violation (or we ran out of the real Aklam Coins :coin:) Feel free to retry!")

                return

            if (paid):
                aklam_coins -= 1

            if (engine == "sd3"):
                with open(fname, 'wb') as file:
                    file.write(response.content)

                fp = open(fname, "rb")

                await message.reply(files=[disnake.File(fp, filename = f"envision.png")])

                fp.close()
                os.unlink(fname)
            elif (engine == "a1111"):
                os.unlink(fname)
            elif (engine == "dalle"):
                revised_prompt = response.data[0].revised_prompt
                image_url = response.data[0].url

                # revised_prompt = "An abstract depiction of the English alphabet letter A. The letter A is glowing with a deep blue hue and a radiating golden outline. Small twinkling stars are scattered around the A, creating a heavenly atmosphere. The background showing a harmonious gradient ranging from the profound black of the cosmic void to the purples and pinks of nebulas. The whole scene gives a sense of tranquillity and wonder, like we're observing a distant celestial event."
                # image_url = "https://oaidalleapiprodscus.blob.core.windows.net/private/org-LVFttHoiiksLmFCDtg17hHss/user-lY1IoPrMgneefaKvCs1k2BGj/img-sfBn8FoMZ4SlBj7IoVSpKanH.png?st=2023-11-09T03%3A34%3A33Z&se=2023-11-09T05%3A34%3A33Z&sp=r&sv=2021-08-06&sr=b&rscd=inline&rsct=image/png&skoid=6aaadede-4fb3-4698-a8f6-684d7786b067&sktid=a48cca56-e6da-484e-a814-9c849652bcb3&skt=2023-11-09T04%3A14%3A26Z&ske=2023-11-10T04%3A14%3A26Z&sks=b&skv=2021-08-06&sig=ePqkeBsKBNA2A27RWDeJmWXatvxvOZuReAqCKdrWR04%3D"

                await msg_response.edit(f"Your prompt has been changed by OpenAI's Envisioner :eye: to **{revised_prompt}**\n\n:loudspeaker: :anger_right: You have **{aklam_coins} Aklam coins** :coin: left")

                r = requests.get(image_url)

                if (r.status_code == 200):
                    with open(fname, "wb") as fp:
                        fp.write(r.content)

                    fp = open(fname, "rb")

                    await message.reply(files=[disnake.File(fp, filename = f"envision.png")])

                    fp.close()
                    os.unlink(fname)
                else:
                    await message.reply("error :(")

        # await message.reply(response.data[0].url)
    elif (inp[0] == "envision_old"):
        msg_error = ""

        # if (message.guild.id != 969621886269030511):
        #     negatives = "(porn:1.1), (nsfw:1.1), " + negatives

        # negatives += "BadDream, (UnrealisticDream:1.2), "

        # if ("pretty" in params):
        #     negative_prompt = f"easynegative, bad-hands-5, {negatives}deformed, disfigured, watermark, signature, (worst quality:2), (low quality:2), (bad anatomy:1.1), bad proportions, bad lighting, (out of frame:1.1), (out of shot:1.1), extra fingers, extra limbs, lowres, blurry, cropped image, black bars, child, toddler"

        #     if (" of " not in query.split(",")[0].lower()):
        #         query = "RAW photography of " + query

        #     prompt = f"{query}, (best quality:1.21), (ultra-detailed:1.21), volumetric lighting, UHD, HDR, <lora:add_detail:{params['lora_detail']}>"
        # elif ("bare" in params):
        #     negative_prompt = f"{negatives}watermark, signature, worst quality, low quality, bad lighting, (out of frame:1.1), (out of shot:1.1), lowres, blurry, cropped image, black bars, child, toddler"
        #     prompt = f"{query}, <lora:add_detail:{params['lora_detail']}>"
        # else:
        #     negative_prompt = f"{negatives}watermark, signature, worst quality, low quality, bad lighting, (out of frame:1.1), (out of shot:1.1), lowres, blurry, cropped image, black bars, child, toddler"

        #     if (" of " not in query.split(",")[0].lower()):
        #         query = "image of " + query

        #     prompt = f"{query}, (best quality:1.21), (ultra-detailed:1.21), <lora:add_detail:{params['lora_detail']}>"

        #prompt = f"{query}, (masterpiece:1.21), (best quality:1.21), (ultra-detailed:1.21), (detailed light:1.1), great composition, breathtaking, volumetric lighting, <lora:epi_noiseoffset2:1>, <lora:add_detail:{params['lora_detail']}>"
        #prompt = f"RAW photography, (highest quality:1.3), (masterpiece:1.21), (photorealistic:1.4), {query}, (high detail:1.21), cinematic composition, volumetric lighting, UHD, HDR, <lora:epi_noiseoffset2:1>, <lora:add_detail:{params['lora_detail']}>"
        #prompt = f"RAW photography, {query}, (photorealistic:1.4), (ultra-detailed:1.21), volumetric lighting, UHD, HDR, <lora:add_detail:{params['lora_detail']}>"
        #prompt = f"{query}, volumetric lighting, UHD, HDR, <lora:add_detail:{params['lora_detail']}>"

        print("prompt:", prompt)
        print("negative prompt:", negative_prompt)

    elif (inp[0] == "rpg_start"):
        if (cores[ch_id].rpg_core is not None):
            msg = "A game is already running! Do ^rpg_end or something"
        else:
            ok = False

            while (not ok):
                ai_return_obj = await get_eleuther_text_direct(
                    rpg_starts["normal"],
                    450,
                    1.05,
                    message.channel
                )

#                 ai_return_obj = {"error": False, "output": """1. Swordsman (1200 HP, 2000 MP, 0.6 ATK): A crafty class with a near-perfect sword arm, these skilled fighters are bold, intelligent and proficient!
# - Attacks: Gladiator blade (55 MP, 45 ATK), Outstanding flash (80 MP, 120 ATK), Unstoppable steel (210 MP, 150 ATK), Rapier blade (285 MP, 200 ATK)
# 2. Human Alchimist (800 HP, 1000 MP, 0.2 ATK): With refined perceptions and keen magical insight, these truly artistic individuals discover that craftsmanship and knowledge, working together, can achieve virtually anything!
# - Attacks: Demonizing shot (60 MP, 40 ATK), Multi-bomb (170 MP, 180 ATK), Pandora spell (200 MP, 200 ATK), Alchemist’s touch (320 MP, 200 ATK)
# 3. Necromancer (850 HP, 3000 MP, 0.2 ATK): A brave necromancer develops an ability to manipulate the dead to attack and defeat enemies, increasing its HP!
# - Attacks: Undead revival (115 MP, 90 ATK), Immortal touch (140 MP, 110 ATK), Expel demons (150 MP, 140 ATK), Soul gathering (270 MP, 150 ATK)
# """}
                print("classes", ai_return_obj["output"])

                if (not ai_return_obj["error"]):
                    rpg_core = RPGCore(uid, ai_return_obj["output"])

                    if (not rpg_core.is_valid()):
                        print("RPG Core is not valid. Retrying.")
                        continue

                    embed = disnake.Embed(title="Select your class!")
                    print(rpg_core.classes)
                    for i, c in enumerate(rpg_core.classes):
                        title = f"{i + 1}. **{c['name']}** (:shield: **{c['hp']} HP**, :fire: **{c['mp']} Max mana**, :crossed_swords: **{c['atk']} Attack multiplier**)"
                        value = f"> _\"{c['description']}\"_\n> \n> Attacks:\n"

                        for a in c["attacks"]:
                            value += f"> - {a['name']} (**{a['atk']}** Damage, **{a['mp']}** Mana cost)\n"

                        if (i % 2 == 0):
                            embed.add_field(name = title, value = value, inline = False)
                        else:
                            embed.add_field(name = title, value = value, inline = True)

                    ok = True
                    cores[ch_id].rpg_core = rpg_core
                else:
                    msg = "an error occured?"
                    ok = True

    elif (inp[0] == "rpg_pick_class"):
        if (cores[ch_id].rpg_core is None):
            msg = "Do ^rpg_start first!"
        elif uid in cores[ch_id].rpg_core.players:
            msg = "You already picked a class! Use ^rpg_me to see what you have, or ^rpg_drop to drop out of the game."
        else:
            try:
                classes = cores[ch_id].rpg_core.classes
                choice = -1
                try:
                    choice = int(inp[1]) - 1
                except:
                    pass

                if choice >= 0 and choice < len(classes):
                    cores[ch_id].rpg_core.add_player(uid, message.author.name, choice)
                    msg = f"You picked class **{classes[choice]['name']}**!"
                else:
                    msg = f"Your choice has to be between 1 and {len(classes)}"
            except:
                traceback.print_exc()
                msg = "Usage: ^rpg_pick_class NUMBER"

    elif (inp[0] == "rpg_drop"):
        if (cores[ch_id].rpg_core is None):
            msg = "Do ^rpg_start first!"
        elif uid not in cores[ch_id].rpg_core.players:
            msg = "You are already out of the game"
        else:
            del cores[ch_id].rpg_core.players[uid]
            msg = "**OK.**"

    elif (inp[0] == "rpg_me"):
        if (cores[ch_id].rpg_core is None):
            msg = "Do ^rpg_start first!"
        elif uid not in cores[ch_id].rpg_core.players:
            msg = "You have to pick a class with ^rpg_pick_class first!"
        else:
            player = cores[ch_id].rpg_core.players[uid]
            description = f"Class: **{player['class']['name']}**\n"
            description += f"Upgrades Available: **{player['upgrades_available']}**\n"
            description += f"Attack multiplier: **{player['class']['atk']}**\n"
            description += f"HP: **{player['hp']}/{player['class']['hp']}**\n"
            description += f"Mana: **{player['mp']}/{player['class']['mp']}**\n"
            description += "\nAttacks:\n"

            for a in player['class']['attacks']:
                description += f"- {a['name']} (**{a['atk']}** Damage, **{a['mp']}** Mana cost)\n"

            embed = disnake.Embed(title=f"{message.author.name}", description = description)
    elif (inp[0] == "rpg_attack"):
        core = cores[ch_id].rpg_core

        if (core is None):
            msg = "Do ^rpg_start first!"
        elif uid not in core.players:
            msg = "You have to pick a class with ^rpg_pick_class first!"
        elif uid != core.get_current_player_turn():
            msg = f"It's not your turn! It's <@{core.get_current_player_turn()}>'s!"
        elif (len(inp) != 3):
            msg = "Usage: ^rpg_attack @player attack_number"
        else:
            target = inp[1]
            print(target)
            target_uid = int(target[2:-1])

            if (target_uid not in core.players):
                msg = "That person is not playing"
            else:
                current_player = core.players[uid]
                target_player = core.players[target_uid]
                # check to not attack yourself?

                attack_index = -1
                try:
                    attack_index = int(inp[2]) - 1
                except:
                    pass

                ok = True
                if (target_player['hp'] <= 0):
                    msg = "The target must be alive!"
                    ok = False

                if (uid == target_uid):
                    msg = "You cannot attack yourself!"
                    ok = False

                if (ok):
                    if (attack_index >= 0 and attack_index < len(current_player['class']['attacks'])):
                        attack = current_player['class']['attacks'][attack_index]
                        if (attack["mp"] <= current_player["mp"]):
                            attack_query = f"player_a ({current_player['class']['name']})"
                            attack_query += f" uses {attack['name']} ({attack['atk']} ATK) on "
                            attack_query += f"player_b ({target_player['class']['name']}): "
                            print(attack_query)

                            ok = False

                            while not ok:
                                print("SENDING", core.attack_log + attack_query)

                                ai_return_obj = await get_eleuther_text_direct(
                                    core.attack_log + attack_query,
                                    100,
                                    1.05,
                                    message.channel
                                )

                                if (not ai_return_obj["error"]):
                                    attack_msg = ai_return_obj["output"].strip()
                                    if ("\n" in attack_msg):
                                        attack_msg = attack_msg[:attack_msg.find("\n")]

                                    # i dont know why it puts so many weird chars at the start
                                    i = 0
                                    for i, c in enumerate(attack_msg):
                                        if (c not in (string.ascii_uppercase + string.ascii_lowercase + string.digits)):
                                            print(i)
                                        else:
                                            break

                                    print("Trimming", i)
                                    attack_msg = attack_msg[i:]

                                    old_target_hp = target_player["hp"]

                                    pre_upgrades_available = current_player['upgrades_available']
                                    print("RECEIVED ATTACK_MSG", attack_msg)
                                    ok = core.player_attack(uid, target_uid, attack_msg, ai_return_obj["query"], attack)
                                    if (ok):
                                        attack_description = attack_msg[:attack_msg.find(". This dealt ")]
                                        attack_description = re.sub("player_a", f"**{current_player['name']}**", attack_description, flags=re.IGNORECASE)
                                        attack_description = re.sub("player_b", f"**{target_player['name']}**", attack_description, flags=re.IGNORECASE)

                                        attack_damage = attack_msg[attack_msg.find(". This dealt ") + len(". This dealt "):]
                                        attack_damage = attack_damage[:attack_damage.find(" damage!")].split(" * ")
                                        print("PRINTED ATTACK_MSG", attack_damage)
                                        attack_value = int(int(attack_damage[0]) * int(attack_damage[1]) * current_player["class"]["atk"])

                                        description = f"_\"{attack_description}\"_\n\n"
                                        description += f"This dealt **{attack_damage[0]} * {attack_damage[1]} * {current_player['class']['atk']} = {attack_value}** Damage!\n\n"
                                        description += f"{target_player['name']}\n"
                                        description += f"> HP: **{old_target_hp}** -> **{target_player['hp']}**\n"

                                        description += "\n"
                                        if (pre_upgrades_available > 0):
                                            description += f":warning: You have **{current_player['upgrades_available']}** upgrades available! Use ^rpg_upgrade.\n"

                                        if (core.winner is not None):
                                            description += f"<@{core.winner}> won the game!"
                                        else:
                                            description += f"It is now <@{core.get_current_player_turn()}>'s turn!"
                                        embed = disnake.Embed(title=f"{current_player['name']} used \"{attack['name']}\" on {target_player['name']}!", description = description)
                        else:
                            msg = "You don't have enough mana for that attack!"
                    else:
                        msg = f"Your attack has to be between 1 and {len(current_player['class']['attacks'])}"

    elif (inp[0] == "rpg_end"):
        if (uid != cores[ch_id].rpg_core.initiator):
            msg = "You need to be the person who started the game in order to end it."
        else:
            cores[ch_id].rpg_core = None
            msg = "**OK.**"

    elif (inp[0] == "rpg_undo_action"):
        if (cores[ch_id].rpg_core is None):
            msg = "Do ^rpg_start first!"
        elif (uid != cores[ch_id].rpg_core.initiator):
            msg = "You need to be the person who started the game."
        else:
            ok = cores[ch_id].rpg_core.undo_action()
            if ok:
                msg = "**OK.**"
            else:
                msg = "You cannot undo more than once :(  (yet?)"

    elif (inp[0] == "rpg_clear_attack_log"):
        if (cores[ch_id].rpg_core is None):
            msg = "Do ^rpg_start first!"
        elif (uid != cores[ch_id].rpg_core.initiator):
            msg = "You need to be the person who started the game."
        else:
            cores[ch_id].rpg_core.attack_log = rpg_hits["normal"]
            msg = "**OK.**"

    elif (inp[0] == "rpg_status"):
        if (cores[ch_id].rpg_core is None):
            msg = "Do ^rpg_start first!"
        else:
            description = ""
            core = cores[ch_id].rpg_core

            if (core.winner is not None):
                description += f"<@{core.winner}> won the game!\n\n"

            for i, player_uid in enumerate(core.players):
                player = core.players[player_uid]
                description += "\\> " if i == core.current_turn else "- "
                description += f"{player['name']} [**{player['score']}** pts]"

                if (i == core.current_turn):
                    description += " **(YOUR TURN!)**"

                description += f"\n> {player['class']['name']}"
                description += f" (**{player['hp']}** / **{player['class']['hp']}** HP, **{player['mp']}** / **{player['class']['mp']}** Mana)\n"

            embed = disnake.Embed(title = f"Game Status (Turn {core.turn})", description = description)

    elif (inp[0] == "rpg_upgrade"):
        if (cores[ch_id].rpg_core is None):
            msg = "Do ^rpg_start first!"
        elif uid not in cores[ch_id].rpg_core.players:
            msg = "You have to pick a class with ^rpg_pick_class first!"
        elif cores[ch_id].rpg_core.players[uid]["upgrades_available"] <= 0:
            msg = "You cannot upgrade anything."
        elif len(inp) < 2:
            description = "Here's what you can upgrade:\n\n"
            description += "- **health**: Call in The Witchdoctor to give your character extra health!\n"
            description += "- **mana**: Use the powers of The Longbearded Mage to increase your maximum mana powers!\n"
            description += "- **attack_multiplier**: Pay a visit to The Blacksmith to get yourself a new weapon!\n"
            description += "- **replenish**: Go to the local Friendly Rat to recover a good part of your HP and mana!\n"
            description += "- **attack [index of one of your attacks]**: Turn one of your attacks into a stronger version of itself! Note: This consumes 2 upgrades.\n"

            embed = disnake.Embed(title = "Welcome to the upgrades section!", description = description)
        else:
            player = cores[ch_id].rpg_core.players[uid]
            query = ""

            run_generic_upgrade = False
            if (inp[1] == "health"):
                run_generic_upgrade = True
                query = rpg_upgrades["normal"]["health"] + f"player ({player['class']['name']}): {player['class']['hp']} HP -> "
            elif (inp[1] == "mana"):
                run_generic_upgrade = True
                query = rpg_upgrades["normal"]["mana"] + f"player ({player['class']['name']}): {player['class']['mp']} MP -> "
            elif (inp[1] == "attack_multiplier"):
                run_generic_upgrade = True
                query = rpg_upgrades["normal"]["attack_multiplier"] + f"player ({player['class']['name']}): {player['class']['atk']} ATK -> "
            elif (inp[1] == "replenish"):
                run_generic_upgrade = True
                query = rpg_upgrades["normal"]["replenish"] + f"player ({player['class']['name']}): ({player['class']['hp']} HP, {player['class']['mp']} MP) -> ("
            elif (inp[1] == "attack"):
                if (player["upgrades_available"] < 2):
                    msg = "You need to have at least 2 upgrades available."
                elif (len(inp) != 3):
                    attacks = player['class']['attacks']
                    msg = "Usage: ^rpg_upgrade attack [index of one of your attacks]\n\n"
                    msg += "You can upgrade the following attacks:\n"

                    for i, a in enumerate(attacks):
                        msg += f"{i + 1}. {a['name']} (**{a['atk']}** Damage, **{a['mp']}** Mana cost)\n"
                else:
                    attacks = player['class']['attacks']

                    choice = -1
                    try:
                        choice = int(inp[2]) - 1
                    except:
                        pass

                    if (choice >= 0 and choice < len(attacks)):
                        a = attacks[choice]
                        query = rpg_upgrades["normal"]["attack"] + f"{player['class']['name']} upgrade: {a['name']} ({a['mp']} MP, {a['atk']} ATK) -> "
                        ok = False
                        while not ok:
                            print("SENDING", query)

                            ai_return_obj = await get_eleuther_text_direct(
                                query,
                                100,
                                1.05,
                                message.channel
                            )

                            if (not ai_return_obj["error"]):
                                upgrade_text = ai_return_obj["output"].strip()
                                if ("\n" in upgrade_text):
                                    upgrade_text = upgrade_text[:upgrade_text.find("\n")]

                                mp = 0
                                atk = 0
                                stats = upgrade_text[upgrade_text.find(" (") + 2:upgrade_text.find(")")].split(", ")
                                print(stats)

                                try:
                                    for stat in stats:
                                        value = float(stat.split()[0])
                                        if (" MP" in stat): mp = int(value)
                                        if (" ATK" in stat): atk = int(value)

                                except:
                                    traceback.print_exc()
                                    print("Skipped invalid stats")
                                    continue

                                if (mp < a['mp'] or atk < a['atk']):
                                    print("Skipped missing or invalid stats")
                                    continue

                                attack_name = upgrade_text[:upgrade_text.find(" (")]
                                upgrade_text = upgrade_text[upgrade_text.find("): ") + 3:]

                                if (attack_name[0] not in string.ascii_letters):
                                    print("Skipping weird attack name", attack_name)
                                    continue

                                old_name = a['name']
                                old_mp = a['mp']
                                old_atk = a['atk']

                                cores[ch_id].rpg_core.player_upgrade_attack(uid, choice, attack_name, mp, atk)
                                description = f"_\"{upgrade_text}\"_\n\n"
                                description += f"**{player['name']}** upgraded their **{old_name}** to **{attack_name}**!\n"
                                description += f"> **{old_mp}** -> **{mp}** Mana cost\n"
                                description += f"> **{old_atk}** -> **{atk}** Damage\n"
                                embed = disnake.Embed(title = "Attack upgrade", description = description)

                                ok = True

                    else:
                        msg = f"Usage: ^rpg_upgrade attack NUMBER\nYour NUMBER has to be between 1 and {len(attacks)}!"

            if (run_generic_upgrade):
                if (len(query) == 0):
                    msg = "Invalid upgrade choice, use ^rpg_upgrade to see available choices!"
                else:
                    ok = False
                    while not ok:
                        print("SENDING", query)

                        ai_return_obj = await get_eleuther_text_direct(
                            query,
                            100,
                            1.05,
                            message.channel
                        )

                        if (not ai_return_obj["error"]):
                            upgrade_text = ai_return_obj["output"].strip()
                            if ("\n" in upgrade_text):
                                upgrade_text = upgrade_text[:upgrade_text.find("\n")]

                            print("RECEIVED", upgrade_text)
                            value_text = ""
                            value_text_2 = ""
                            old_value = 1
                            old_value_2 = 1

                            if (inp[1] == "health"):
                                value_text = upgrade_text[:upgrade_text.find(" HP: ")]
                                old_value = player['class']['hp']

                            if (inp[1] == "mana"):
                                value_text = upgrade_text[:upgrade_text.find(" MP: ")]
                                old_value = player['class']['mp']

                            if (inp[1] == "attack_multiplier"):
                                value_text = upgrade_text[:upgrade_text.find(" ATK: ")]
                                old_value = player['class']['atk']

                            if (inp[1] == "replenish"):
                                value_text = upgrade_text[:upgrade_text.find(" HP")]
                                value_text_2 = upgrade_text[upgrade_text.find(" HP, ") + 5:upgrade_text.find(" MP")]
                                old_value = player['class']['hp']
                                old_value_2 = player['class']['mp']

                            value = 0
                            value_2 = 0
                            try:
                                value = float(value_text)
                                if (inp[1] == "replenish"):
                                    value_2 = float(value_text_2)
                            except:
                                traceback.print_exc()
                                continue

                            if (value <= old_value):
                                print("invalid value", value, old_value)
                            elif (inp[1] == "replenish" and value_2 <= old_value_2):
                                print("invalid value_2", value_2, old_value_2)
                            else:
                                upgrade_text = re.sub("the player", f"**{player['name']}**", upgrade_text, flags=re.IGNORECASE)
                                upgrade_text = re.sub("player", f"**{player['name']}**", upgrade_text, flags=re.IGNORECASE)

                                if (inp[1] == "health"):
                                    value = int(value)
                                    cores[ch_id].rpg_core.player_upgrade_health(uid, value)
                                    description = f"_\"{upgrade_text[upgrade_text.find(' HP: ') + 5:]}\"_\n\n"
                                    description += f"**{player['name']}**'s Max HP went from **{old_value}** to **{value}** :shield:!"
                                    embed = disnake.Embed(title = "Health upgrade", description = description)

                                if (inp[1] == "mana"):
                                    value = int(value)
                                    cores[ch_id].rpg_core.player_upgrade_mana(uid, value)
                                    description = f"_\"{upgrade_text[upgrade_text.find(' MP: ') + 5:]}\"_\n\n"
                                    description += f"**{player['name']}**'s Max Mana went from **{old_value}** to **{value}** :fire:!"
                                    embed = disnake.Embed(title = "Mana upgrade", description = description)

                                if (inp[1] == "attack_multiplier"):
                                    cores[ch_id].rpg_core.player_upgrade_attack_multiplier(uid, value)
                                    description = f"_\"{upgrade_text[upgrade_text.find(' ATK: ') + 6:]}\"_\n\n"
                                    description += f"**{player['name']}**'s Attack multiplier went from **{old_value}** to **{value}** :crossed_swords:!"
                                    embed = disnake.Embed(title = "Attack upgrade", description = description)

                                if (inp[1] == "replenish"):
                                    cores[ch_id].rpg_core.player_upgrade_replenish(uid, value, value_2)
                                    description = f"_\"{upgrade_text[upgrade_text.find('): ') + 3:]}\"_\n\n"
                                    description += f"**{player['name']}** heals **{value}** HP and regenerates **{value_2}** mana!"
                                    embed = disnake.Embed(title = "Replenish", description = description)

                                ok = True

    elif (inp[0] == "chess"):
        ci = raw_inp.split(" ")[1]
        cores[ch_id].chess.append(ci)
        moves = cores[ch_id].chess

        query = "Here is a transcript of a chess match:\n"
        total_turns = len(moves) // 2 + 1
        for i in range(0, len(moves), 2):
            query += str((i // 2) + 1) + ". " + moves[i] + " "
            if (i + 1 < len(moves)):
                query += moves[i + 1] + " "

        print("Sending: " + query)
        move = ""
        while(len(move) == 0):
            ai_return_obj = await get_eleuther_text(query, cores[ch_id])

            move = ai_return_obj["output"].strip()
            stop_symbol = str(total_turns + 1) + ". "

            if (move.find(stop_symbol) == -1):
                move = ""
            else:
                move = move[:move.find(stop_symbol)].strip()

        cores[ch_id].chess.append(move)
        msg = move

    elif (inp[0] == "core_gen_voice"):
        if (len(inp) < 2):
            msg = f"Usage: {prefix}core_gen_voice voice_type message"
        else:
            voice = inp[1]
            if voice not in voices.voices:
                msg = f"Usage: {prefix}core_gen_voice voice_type message\nVoices available: {list(voices.voices.keys())}"
            else:
                fname = f"tmp/{message.id}.mp3"
                if (len(message.attachments) > 0 and message.attachments[0].content_type):
                    fname_tmp = f"tmp/{message.id}_1.mp3"
                    media.audio_url_to_file(message.attachments[0].proxy_url, fname_tmp)
                    voices.speech_to_speech(fname_tmp, fname, voice)
                    os.remove(fname_tmp)
                else:
                    text = raw_inp[raw_inp.find(voice) + len(voice) + 1:]

                    if (cores[ch_id].gen_voice_use_openai):
                        supported_personalities = {"junko_queen": "junko"}
                        if (voice in supported_personalities or voice in chat.personality_core_templates):
                            messages = []

                            if (voice in supported_personalities):
                                personality_name = supported_personalities[voice]
                            else:
                                personality_name = voice
                            personality_core = chat.personality_core_templates[personality_name].clone()

                            content = []

                            if (isinstance(personality_core.header, str)):
                                content.append({"type":"text", "text": personality_core.header})
                            else:
                                for line in personality_core.header:
                                    if (line["type"] == "text"):
                                        content.append({"type": "text", "text": line["content"]})
                                    elif (line["type"] == "audio"):
                                        content.append({"type": "input_audio", "input_audio": {"data": line["content"], "format": "mp3"}})

                            messages.append({"role": "user", "content": content})
                            messages.append({"role": "system", "content":[{"type":"text", "text":f"Ok! so you've kinda thought of something to say, it's: \"{text}\". Remember it's SUPER important to talk exactly like how your character would talk! Loads of expressiveness (or complete lack of, if it's in character) is super super important, ur friends will absolutely love it!"}]})

                            # messages.append({"role": "system", "content": {"type": "text", "text": f"Okay! Soo, since now you know how u talk and stuff, I'm now going to give you a fun transcript to read! All you gotta do is say it SUPER EXTPRESSIVELY, in character, from start to finish!"}})
                            # messages.append({"role": "assistant", "content": {"type": "text", "text": f"Ok! I'm ready :D"}})
                            # messages.append({"role": "system", "content": {"type": "text", "text": f"Awesome, so here it is:\n{text}"}})
                            # messages.append({"role": "assistant", "content": {"type": "text", "text": f"Ok! I start:"}})

                            out = await get_openai_response(messages, "gpt-4o-audio-preview", 1200, 1.0, 0.5, True, personality_core.voice_style)
                            out_audio = out["audio"]

                            fname_tmp = f"tmp/{message.id}_1.mp3"
                            with open(fname_tmp, "wb") as fp:
                                fp.write(out_audio)

                            voices.speech_to_speech(fname_tmp, fname, voice)
                            os.remove(fname_tmp)
                        else:
                            voices.text_to_speech(text, fname, voice)
                    else:
                        voices.text_to_speech(text, fname, voice)

                with open(fname, "rb") as fp:
                    response_audio = fp.read()

                os.remove(fname)
    elif (inp[0] == "core_gen_voice_next"):
        if (len(inp) < 2):
            msg = f"Usage: {prefix}core_gen_voice_next voice_type"
        else:
            voice = inp[1]
            if voice not in voices.voices:
                msg = f"Voices available: {list(voices.voices.keys())}"
            else:
                core_gen_voice_next = True
                core_gen_voice_next_voice = voice
                msg = f"Next voice message will be speech to speech converted to **{voice}**!"

    elif (core_gen_voice_next and len(message.attachments) > 0 and message.attachments[0].content_type):
        core_gen_voice_next = False
        voice = core_gen_voice_next_voice
        if voice not in voices.voices:
            msg = f"Voice chosen {voice} is invalid, voices available: {list(voices.voices.keys())}"
        else:
            fname = f"tmp/{message.id}.mp3"
            fname_tmp = f"tmp/{message.id}_1.mp3"
            media.audio_url_to_file(message.attachments[0].proxy_url, fname_tmp)
            voices.speech_to_speech(fname_tmp, fname, voice)
            os.remove(fname_tmp)

            with open(fname, "rb") as fp:
                response_audio = fp.read()

            os.remove(fname)

    elif (inp[0].startswith("core_")):
        msg = f"Invalid command! Do {prefix}core_help."

    elif (inp[0] == ("generate_raw")):
        msg = f"Invalid command. It's now {prefix}core_gen"

    else:
        if (cores[ch_id].gpt_engine in []):
            msg = f"Chatting is shut down because chatting is way too expensive lol. Use {prefix}core_gen!"
        else:
            if (uid in shadowban):
                raw_inp = random.choice(["do you like the weather?", "do you love the weather?", "what do you think about the weather?"])

            print("happening")
            await cores[ch_id].prime_for_ai(message.author.name, raw_inp, message.id, message.attachments)

            ai_return_obj = await get_eleuther_text(cores[ch_id].query, cores[ch_id], "chat")

            response = ai_return_obj["output"].strip()
            response_audio = ai_return_obj["out_audio"]

            if (response[-1] == '"'):
                response = response[:-1]

            chatted = True

            msg = response

    if (embed is not None):
        await message.reply(embed = embed)
    elif (len(msg) > 0 or response_audio is not None):
        if (response_audio is not None):
            fname = f"tmp/{message.id}.mp3"

            if (chatted and cores[ch_id].second_voice is not None):
                fname_tmp = f"tmp/{message.id}_1.mp3"
                with open(fname_tmp, "wb") as fp:
                    fp.write(response_audio)

                voices.speech_to_speech(fname_tmp, fname, cores[ch_id].second_voice)
                os.remove(fname_tmp)
            else:
                with open(fname, "wb") as fp:
                    fp.write(response_audio)

            fp = open(fname, "rb")

            if (chatted and cores[ch_id].gpt_engine in ["claude-3-5-sonnet", "claude-3-7-sonnet"]):
                msg_clean = msg.replace("\n\n", "\n")
                m = await message.reply(f"||{msg_clean}||", file = disnake.File(fname))
            else:
                m = await message.reply(file = disnake.File(fname))

            fp.close()
            os.remove(fname)
        else:
            m = await message.reply(msg)

        print(chatted)

        if (chatted):
            if ("audio_transcript" in ai_return_obj and ai_return_obj["audio_transcript"] is not None):
                msg = ai_return_obj["audio_transcript"]

            cores[ch_id].ai_responded(ai_return_obj["query"], msg, m.id, error = ai_return_obj["error"], msg_audio = response_audio)

dirs = ["/tmp"]

for d in dirs:
    if (not os.path.isdir(getPath() + d)):
        os.mkdir(getPath() + d)

#openai.api_key = open("openai_token").read().strip()
bot.run(open("token").read().strip())
