import ffmpeg
import requests
import os, base64

audio_cache = {}
max_cache = 50

def get_audio_file(fname):
    with open(fname, "rb") as fp:
        audio_data = fp.read()

    return audio_data

def audio_file_to_base64(fname):
    audio_data = get_audio_file(fname)
    return base64.encodebytes(audio_data).decode().replace("\n", "")

def audio_url_to_file(audio_url, fname):
    audio_data = requests.get(audio_url).content

    with open(fname, "wb") as fp:
        fp.write(audio_data)

def audio_url_to_mp3_base64(audio_id, audio_url):
    global audio_cache

    if audio_id not in audio_cache:
        audio_data = requests.get(audio_url).content
        fname = f"tmp/{audio_id}"
        fname_out = f"tmp/{audio_id}.mp3"

        with open(fname, "wb") as fp:
            fp.write(audio_data)

        print("started")
        ffmpeg_obj = ffmpeg.input(fname)
        process = ffmpeg_obj.output(fname_out).run_async(quiet = True, overwrite_output = True)
        process.communicate()
        print("finished!")

        os.remove(fname)

        with open(fname_out, "rb") as fp:
            mp3_data = fp.read()

        os.remove(fname_out)

        mp3_data = base64.encodebytes(mp3_data).decode().replace("\n", "")
        audio_cache[audio_id] = mp3_data
    else:
        mp3_data = audio_cache[audio_id]

    print("audio_cache size", len(audio_cache))

    # prune cache:
    if (len(audio_cache) > max_cache):
        prune_keys = list(audio_cache.keys())[:-max_cache]
        for key in prune_keys:
            del audio_cache[key]

    return mp3_data
