from enum import Enum
from chat import *
from rpg import *
import asyncio
import media
import base64

class Core:
    def __init__(self, personality_core_name, gpt_engine, discord_channel):
        if (personality_core_name not in personality_core_templates):
            personality_core_name = "malka_experiment_shitposting"

        self.voice_mode = False
        self.voice_type = "ballad"
        self.second_voice = None
        self.gpt_engine = gpt_engine
        self.discord_channel = discord_channel
        self.gen_voice_use_openai = True
        self.set_personality_core(personality_core_name)
        self.reset()

        self.global_settings = {

        }

        self.first_run = True

        default_output_size = 125

        default_gen_core_temp = 0.85
        default_chat_core_temp = 0.87

        self.settings = {
            # "gpt-j-eleuther": {
            #     "raw": {
            #         "temperature": default_gen_core_temp,
            #         "output_size": default_output_size
            #     },

            #     "chat": {
            #         "temperature": default_chat_core_temp,
            #         "output_size": default_output_size,
            #         "chat_length": 500
            #     }
            # },

            # "gpt-j-forefront": {
            #     "raw": {
            #         "temperature": default_gen_core_temp,
            #         "output_size": default_output_size,
            #         "repetition_penalty": 0
            #     },

            #     "chat": {
            #         "temperature": default_chat_core_temp,
            #         "output_size": default_output_size,
            #         "repetition_penalty": 1.4,
            #         "chat_length": 500
            #     }
            # },

            # "gpt-20b": {
            #     "raw": {
            #         "temperature": default_gen_core_temp,
            #         "output_size": 150
            #     },

            #     "chat": {
            #         "temperature": default_chat_core_temp,
            #         "output_size": default_output_size,
            #         "chat_length": 500
            #     }
            # },

            # "gpt-3": {
            #     "raw": {
            #         "temperature": 1.1,
            #         "output_size": default_output_size,
            #         "repetition_penalty": 0
            #     },

            #     "chat": {
            #         "temperature": 1.0, #1.3
            #         "output_size": default_output_size,
            #         "chat_length": 15,
            #         "repetition_penalty": 0
            #     }
            # },

            "gpt-3.5": {
                "raw": {
                    "temperature": 1,
                    "output_size": 250,
                    "repetition_penalty": 0.5
                },

                "chat": {
                    "temperature": 1.1,
                    "output_size": default_output_size,
                    "chat_length": 20,
                    "repetition_penalty": 0.5
                }
            },

            "gpt-4o-mini": {
                "raw": {
                    "temperature": 1,
                    "output_size": 500,
                    "repetition_penalty": 0.5
                },

                "chat": {
                    "temperature": 1.1,
                    "output_size": 1500,
                    "chat_length": 20,
                    "repetition_penalty": 0.5
                }
            },

            "gpt-4o": {
                "raw": {
                    "temperature": 1,
                    "output_size": 500,
                    "repetition_penalty": 0.5
                },

                "chat": {
                    "temperature": 1.1,
                    "output_size": 1500,
                    "chat_length": 25,
                    "repetition_penalty": 0.5
                }
            },

            "gpt-4.5": {
                "raw": {
                    "temperature": 1,
                    "output_size": 500,
                    "repetition_penalty": 0.5
                },

                "chat": {
                    "temperature": 1.1,
                    "output_size": 1500,
                    "chat_length": 25,
                    "repetition_penalty": 0.5
                }
            },

            # "chat-gpt": {
            #     "raw": {
            #         "temperature": default_gen_core_temp,
            #         "output_size": 150
            #     },

            #     "chat": {
            #         "temperature": default_chat_core_temp,
            #         "output_size": default_output_size,
            #         "chat_length": 500
            #     }
            # },

            "claude-3-5-sonnet": {
                "raw": {
                    "temperature": 1,
                    "output_size": 500,
                    "repetition_penalty": 0
                },

                "chat": {
                    "temperature": 0.6,
                    "output_size": 1500,
                    "chat_length": 25,
                    "repetition_penalty": 0
                }
            },

            "claude-3-7-sonnet": {
                "raw": {
                    "temperature": 1,
                    "output_size": 500,
                    "repetition_penalty": 0
                },

                "chat": {
                    "temperature": 0.6,
                    "output_size": 1500,
                    "chat_length": 25,
                    "repetition_penalty": 0
                }
            }
        }

        self.last_message_index = 0
        self.currently_processing_message_index = 0

        self.rpg_core = None
        self.chess = []
        self.core_pre_gen = {}

        self.messages = []

    def append_pre_gen(self, uid, text):
        if (uid not in self.core_pre_gen):
            self.core_pre_gen[uid] = ""

        self.core_pre_gen[uid] += text

    def reset_pre_gen(self, uid):
        self.core_pre_gen[uid] = ""

    def get_pre_gen(self, uid):
        if (uid not in self.core_pre_gen):
            self.core_pre_gen[uid] = ""

        return self.core_pre_gen[uid]

    def get_settings(self):
        return self.settings[self.gpt_engine]

    def get_temperature(self, mode):
        return self.settings[self.gpt_engine][mode]["temperature"]

    def get_repetition_penalty(self, mode):
        if "repetition_penalty" in self.settings[self.gpt_engine][mode]:
            return self.settings[self.gpt_engine][mode]["repetition_penalty"]
        else:
            return 0.25

    def get_output_size(self, mode):
        return self.settings[self.gpt_engine][mode]["output_size"]

    def set_personality_core(self, personality_core_name):
        self.personality_core_id = personality_core_name
        self.personality_core = personality_core_templates[personality_core_name].clone()
        if (self.personality_core.voice_style is not None):
            self.voice_type = self.personality_core.voice_style

        self.second_voice = self.personality_core.second_voice

        self.personality_core.initiate("test")
        self.reset()

    async def prime_for_ai(self, message_author_name, message, message_id, attachments):
        current_message_index = self.last_message_index
        self.last_message_index += 1

        while (current_message_index > self.currently_processing_message_index):
            #print(f"Chat busy... {self.currently_processing_message_index}/{current_message_index}")
            await asyncio.sleep(0.1)

            if (current_message_index > self.last_message_index):
                print("was reset.")
                return

        self.messages.append({"author_name": message_author_name, "author_type": "user", "content": message, "id": message_id, "attachments": attachments})
        memory_size = int(self.settings[self.gpt_engine]["chat"]["chat_length"])

        self.query = []

        if (self.personality_core.header is not None):
            if (isinstance(self.personality_core.header, str)):
                self.query.append({"author_type": "system", "content": [{"type": "text", "content": self.personality_core.header}]})
            else:
                self.query.append({"author_type": "user", "content": self.personality_core.header})

        for m in self.messages[-memory_size:]:
            content = []

            if ("injected_audio" in m and m["injected_audio"] is not None):
                content.append({"type": "audio", "content": m["injected_audio"], "transcript": m["content"]})
            else:
                if (len(m["content"]) > 0):
                    content.append({"type": "text", "content": m["content"]})

                if (len(m["attachments"]) > 0):
                    for att in m["attachments"]:
                        if ("audio/" in att.content_type):
                            content.append({"type": "audio", "content": media.audio_url_to_mp3_base64(att.id, att.proxy_url)})

            self.query.append({"author_type": m["author_type"], "author_name": m["author_name"], "content": content})

        if (self.personality_core.footer is not None):
            self.query.append({"author_type": "system", "content": [{"type": "text", "content": self.personality_core.footer}]})

        # if (self.gpt_engine not in ["gpt-3.5", "claude-3-opus"]):
        #     message = message.split("\n")[0]
        #     self.history += message_author_name + f": \"{message}\"\n"
        #     self.history += self.personality_core.name + ": \""

        print("PRIME")

        for m in self.query:
            print(m["author_type"], end = " ")
            if ("author_name" in m):
                print(m["author_name"], end = " ")

            for c in m["content"]:
                if (c["type"] == "text"):
                    print(c["content"])
                elif (c["type"] == "audio"):
                    print(c["content"][:32])
                    if ("transcript" in c):
                        print("transcript", c["transcript"])

    def ai_responded(self, query, message, message_id, msg_audio = None, error = False):
        if (error):
            self.messages = self.messages[:-1]
        else:
            if (msg_audio is not None):
                self.messages.append({"author_type": "user", "author_name": self.personality_core.name, "content": message, "id": message_id, "attachments": [], "injected_audio": base64.encodebytes(msg_audio).decode().replace("\n", "")})
            else:
                self.messages.append({"author_type": "assistant", "author_name": self.personality_core.name, "content": message, "id": message_id, "attachments": [], "injected_audio": None})

        self.currently_processing_message_index += 1

    def chat_undo(self, amount):
        if (amount >= len(self.messages)):
            self.messaages = []
        else:
            self.messages = self.messages[:-amount]

    def chat_get_last_msg(self):
        if (len(self.messages) > 0):
            return self.messages[-1]
        else:
            return None

    def chat_edit_msg(self, message_id, new_msg):
        print("messages", self.messages)

        for m in self.messages:
            print("msgs:", m["id"], message_id)
            if (m["id"] == message_id):
                m["content"] = new_msg
                return True

        return False

    def reset(self):
        self.last_message_index = 0
        self.currently_processing_message_index = 0
        self.chess = []
        self.first_run = True
        self.messages = []