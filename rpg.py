import traceback
import copy

class RPGCore:
    def __init__(self, initiator, classes_text, rpg_type):
        self.classes = self.parse_classes(classes_text)
        self.players = {}
        self.current_turn = 0 # which player is rn
        self.attack_log = rpg_hits[rpg_type]["normal"]
        self.initiator = initiator
        self.last_self = None
        self.turn = 1 # Number of times current_turn rolled over
        self.max_turns = 10
        self.winner = None
        self.rpg_type = rpg_type

    def is_valid(self):
        return self.classes is not None

    def get_current_player_turn(self):
        if (self.current_turn >= len(self.players)):
            return None

        return list(self.players.keys())[self.current_turn]

    def add_player(self, uid, name, class_id):
        self.players[uid] = {"name": name, "class": copy.deepcopy(self.classes[class_id]), "level": 1, "upgrades_available": 0, "score": 0, "immunity": False}
        self.players[uid]["hp"] = self.players[uid]["class"]["hp"]
        self.players[uid]["mp"] = self.players[uid]["class"]["mp"]

        print(self.players)

    def undo_action(self):
        if (self.last_self is not None):
            self.players = self.last_self.players
            self.current_turn = self.last_self.current_turn
            self.attack_log = self.last_self.attack_log
            self.turn = self.last_self.turn
            self.winner = self.last_self.winner
            self.last_self = None
            return True
        else:
            return False

    def player_upgrade_health(self, upgrader, new_max_hp):
        self.last_self = copy.deepcopy(self)
        player = self.players[upgrader]

        hp_ratio = float(player["hp"]) / player["class"]["hp"]
        player["class"]["hp"] = new_max_hp
        player["hp"] = int(new_max_hp * hp_ratio)
        player["upgrades_available"] -= 1

    def player_upgrade_mana(self, upgrader, new_max_value):
        self.last_self = copy.deepcopy(self)
        player = self.players[upgrader]

        ratio = float(player["mp"]) / player["class"]["mp"]
        player["class"]["mp"] = new_max_value
        player["mp"] = int(new_max_value * ratio)
        player["upgrades_available"] -= 1

    def player_upgrade_attack_multiplier(self, upgrader, new_max_value):
        self.last_self = copy.deepcopy(self)
        player = self.players[upgrader]

        player["class"]["atk"] = new_max_value
        player["upgrades_available"] -= 1

    def player_upgrade_attack(self, upgrader, a_id, name, mp, atk):
        self.last_self = copy.deepcopy(self)
        player = self.players[upgrader]

        player["class"]["attacks"][a_id]["name"] = name
        player["class"]["attacks"][a_id]["mp"] = mp
        player["class"]["attacks"][a_id]["atk"] = atk
        player["upgrades_available"] -= 1

    def player_upgrade_replenish(self, upgrader, hp, mp):
        self.last_self = copy.deepcopy(self)
        player = self.players[upgrader]

        player["hp"] += hp
        player["mp"] += mp

        if (player["hp"] > player["class"]["hp"]):
            player["hp"] = player["class"]["hp"]

        if (player["mp"] > player["class"]["mp"]):
            player["mp"] = player["class"]["mp"]
        
        player["upgrades_available"] -= 1

    def player_attack(self, attacker, target, attack_text, new_attack_log, attack):
        self.last_self = copy.deepcopy(self)

        attack_damage_text = attack_text[attack_text.find(". This dealt ") + len (". This dealt "):]
        attack_damage_text = attack_damage_text[:attack_damage_text.find(" damage!")].split(" * ")
        print("ATTACK_DMG_TEXT", attack_damage_text)
        
        attack_damage = 0
        try:
            attack_damage = int(attack_damage_text[0]) * int(attack_damage_text[1])
        except:
            traceback.print_exc()
            return False
        
        if (int(attack_damage_text[0]) != attack['atk']):
            print("Wrong attack value")
            return False

        if (int(attack_damage_text[1]) == 0):
            print("Attack multiplier is 0? :(")
            return False

        attacker_player = self.players[attacker]
        target_player = self.players[target]
        
        attack_damage *= attacker_player["class"]["atk"]
        attack_damage = int(attack_damage)
        
        attacker_player['score'] += attack_damage

        print("ATTACK_DMG", attack_damage)
        target_player["hp"] -= attack_damage
        if (target_player["hp"] <= 0):
            target_player["hp"] = 0
            target_player["score"] = int(target_player["score"] * 2/3)

        self.next_turn()
        
        attacker_player["mp"] -= attack["mp"]
        self.attack_log = new_attack_log + attack_text + "\n- "
        return True

    def next_turn(self):
        ok = False
        
        while not ok:
            self.current_turn += 1
            if (self.current_turn >= len(self.players)):
                self.turn += 1
                self.current_turn = 0
                
                for p in self.players:
                    player = self.players[p]
                    player["upgrades_available"] += 2

                if (self.turn >= self.max_turns):
                    max_score = 0
                    for p in self.players:
                        player = self.players[p]
                        if player['score'] >= max_score:
                            self.winner = p
                            max_score = player['score']

            print("Turn:", self.current_turn)

            player = self.players[list(self.players.keys())[self.current_turn]]

            if (player["hp"] != 0):
                player["immunity"] = False
                ok = True

            if (player["hp"] == 0):
                player["hp"] = player["class"]["hp"]
                player["mp"] = player["class"]["mp"]
                player["immunity"] = True

    def parse_classes(self, classes_text):
        lines = classes_text.split("\n")

        i = 0
        classes = []
        current_class = {}
        while i < len(lines):
            line = lines[i]

            if ("): " in line):
                class_name = line[line.find(". ") + 1:line.find(" (")].strip()
                
                stats = line[line.find(" (") + 2:line.find("): ")].split(", ")
                print(stats)
                hp = 0
                mp = 0
                atk = 0
                
                try:
                    for stat in stats:
                        value = float(stat.split()[0])
                        if (" HP" in stat): hp = int(value)
                        if (" MP" in stat): mp = int(value)
                        if (" ATK" in stat): atk = value
                            
                except:
                    traceback.print_exc()
                    print("Skipped invalid stats")
                    i += 2
                    continue
                
                if (hp <= 0 or mp <= 0 or atk <= 0):
                    print("Skipped missing stats")
                    i += 2
                    continue

                class_desc = line[line.find("): ") + 3:]
                print(class_name, hp, mp, atk)
                print(class_desc)

                current_class = {"name": class_name, "description": class_desc, "hp": hp, "mp": mp, "atk": atk}
            elif ("- Attacks: " in line and len(current_class) != 0):
                line = line[len("- Attacks: "):].split("), ")
                current_class["attacks"] = []

                for attack in line:
                    if (attack.find(" (") != -1):
                        attack_name = attack[:attack.find(" (")]
                        
                        stats = attack[attack.find(" (") + 2:].split(", ")
                        print(stats)

                        atk = -1
                        mp = -1
                        try:
                            for stat in stats:
                                value = float(stat.split()[0])
                                if (" MP" in stat): mp = int(value)
                                if (" ATK" in stat): atk = int(value)
                                    
                        except:
                            traceback.print_exc()
                            print("Skipped class's attack:", attack)
                            continue
                            
                        if (mp <= 0 or atk <= 0):
                            print("Skipped missing attack stats")
                            continue

                        current_class["attacks"].append({"name": attack_name, "atk": atk, "mp": mp})

                if (len(current_class["attacks"]) != 0):
                    classes.append(current_class)
                
                current_class = {}
            i += 1

        if (len(classes) > 1):
            return classes
        else:
            return None
            
rpg_starts = {
    "pvp_die_once": {"normal": """Funny RPG classes:
1. Archer (2400 HP, 600 MP, 0.4 ATK): This nimble class uses a bow and arrow to shoot down its enemies with striking precision!
- Attacks: Aim and shoot (50 MP, 30 ATK), Bullseye shot (65 MP, 80 ATK), Rain of doom (200 MP, 150 ATK), Gatling arrows (290 MP, 200 ATK)
2. Chicken (1500 HP, 800 MP, 0.6 ATK): A curious chicken who has a fresh appetite for seeds, blood and destruction!
- Attacks: Mega cluck (65 MP, 50 ATK), Lay egg (200 MP, 100 ATK), Flocking chaos (210 MP, 140 ATK), Explosive feathers (300 MP, 190 ATK)
3. """}
}

rpg_hits = {
    "pvp_die_once": {"normal": """- player_a (Archer) uses Aim and shoot (30 ATK) on player_b (Warrior): player_a extends their compound bow and take aim. They release [1] high speed arrow which hits player_b straight in the chest. This dealt 30 * 1 damage!
- player_a (Ninja) uses Shouriken rain (45 ATK) on player_b (Paladin): player_a comes out of the shadows at dizzying speed, launching [3] shurikens at player_b. This dealt 45 * 3 damage!
- player_a (Cat) uses Meow of death (120 ATK) on player_b (Archer): player_a starts hissing at player_b, and player_a's eyes are slowly turning into a shade of blood red. player_a meows [2] times, producting a powerful shockwave which blasts player_b back. This dealt 120 * 2 damage!
- player_a (Washing Machine) uses Whirlpool of destruction (195 ATK) on player_b (Juggernaut): player_a begins spinning rapidly, vibrating with an incredible force. player_a creates [1] gigantic whirlpool which torrents above player_b. This dealt 195 * 1 damage!
- player_a (Ninja) uses Shouriken rain (45 ATK) on player_b (Paladin): player_a comes out of the shadows at dizzying speed, launching [3] shurikens at player_b. This dealt 45 * 3 damage!
- player_a (Mechanoid) uses Anodized bomb (92 ATK) on player_b (Cat): player_a takes out [4] ticking bombs from their robotic internals and launches them at player_b. The bombs release intensely colored fumes and they detonate, inflicting player_b with reactive anodized isotopes. This dealt 92 * 4 damage!
- """}
}

rpg_upgrades = {
    "pvp_die_once": {"normal": {
        "health": """- Archer: 3000 HP -> 3780 HP: The Witchdoctor takes a mix of herbal medicine and wraps it around your wrists. You feel the leaves bind to you, giving you a boost in your max health!
- Tanker: 1200 HP -> 1920 HP: The Witchdoctor takes out a gigantic syringe full of nutrients and injects your arm with it. Your health increases considerably as you feel your heart rate triple.
- Cat: 1640 HP -> 2300 HP: The Witchdoctor gives you a bowl full of sparkly and nourishing cat food. You eat in glee and munch on the tuna-flavoured cat food, as your max health is being increased.
- Mechanoid: 2100 HP -> 2840 HP: The Witchdoctor looks in his cupboard full of robotic parts and takes out a few gears and scrap metal. He installs a few extra pistons and a hard metal plate on you, increasing your health slightly.
- Holy Knight: 1900 HP -> 2630 HP: The Witchdoctor opens his old book of holy magic, and casts a divine reinforcement spell on you. Angels start singing as your maximum health increases.
- """,
        "mana": """- Archer: 800 MP -> 1280 MP: The Longbearded Mage dips you in a cauldron full of a shimmery blue liquid. This concoction is sparkling fireworks of pure mana. You feel how your mana capacity increases considerably.
- Tanker: 1100 MP -> 1500 MP: The Longbearded Mage takes a magically infused sharktooth. He pinches your arm and you yelp as you see mana blood starting to fill the sharktooth's cavity. You feel your maximum mana increase.
- Cat: 1350 MP -> 1670 MP: The Longbearded Mage puts you on a large experiment table. He casts a wall-shattering spell of strength and magic, rising you in the air. You meow as your eyes turn white and feel a huge influx of magic, which increases your maximum mana considerably.
- Mechanoid: 600 MP -> 1050 MP: The Longbearded Mage takes out a fluxed mana crystal energy pack for you. He opens up your backplate and links up the new module, which increases your maximum mana supply.
- Holy Knight: 2000 -> 2600 MP: The Longbearded Mage summons a ball of pure light, which rivals the power of the sun. He manages to contain its entire energy into an overcharged heavenly mana potion, which you drink. This increases your maximum mana by a lot.
- """,
        "attack_multiplier": """- Archer: 0.5 ATK -> 0.6 ATK: The Blacksmith crafts a set of 10 deadly fiery arrows, cast from extremely strong molten Blackiron. You look in awe as you equip the new set of arrows, which increases your attack multiplier!
- Tanker: 1.3 ATK -> 1.4 ATK: The Blacksmith studies your current arsenal of weapons. He crafts a state-of-the-art 2-meter long blade, with blood coursing through it. You smile and mounts the new blade, your attack multiplier increasing considerably.
- Cat: 2.4 ATK -> 2.45 ATK: The Blacksmith smirks at your cute appearance, then goes to his anvil and crafts a few deadly damascus steel cat claws. He mounts these on your paws, raising your attack multiplier.
- Mechanoid: 1.8 ATK -> 1.9 ATK: The Blacksmith starts up their experimental fusion reactor, and melts down a powerful nuclear alloy. You use this alloy to enrich your mechanoid metal claws, increasing your attack multiplier.
- Holy Knight: 0.8 ATK -> 0.95 ATK: The Blacksmith reaches for the finest grade silver he can get. He casts it into an extremely sharp sword, which shines in the heavenly skies. You takes this sword and increase your attack multiplier.
- """,
        "replenish": """- Archer: (800 HP, 400 MP) -> (420 HP, 250 MP): The Friendly Rat offers you a nice warm tea, replenishing some of your health and mana.
- Tanker: (1800 HP, 900 MP) -> (950 HP, 600 MP): The Friendly Rat takes a big old tankard of freshly poured cold beer for your enjoyment, which you drink with glee. You wipe off the beer from your mouth and recover a bunch of your health and mana!
- Cat: (2400 HP, 1300 MP) -> (1300 HP, 750 MP): The Friendly Rat gives you a bowl full of delicious cat food. You munch away and regenerate health and mana.
- Mechanoid: (3000 HP, 2100 MP) -> (1800 HP, 1300 MP): The Friendly Rat pours a stream of luscious motor oil into a red canister. You drink it all to the last drop and let out a mechanized laugh. You replenished some of your health and mana!
- """,
        "attack": """- Archer upgrade: Aim and shoot (50 MP, 30 ATK) -> Aim, shoot and incinerate (60 MP, 80 ATK): You smile as your arrows become ablaze with a nice tinge of purple fire.
- Chicken upgrade: Mega cluck (200 MP, 160 ATK) -> Giga cluck (225 MP, 250 ATK): You let out a terrifying screech, as your clucking powers increase.
- Mecha Robot upgrade: Mechanoid genome (220 MP, 190 ATK) -> Computerized mechanoid RNA genome (245 MP, 255 ATK): You self-install a next generation computer core, which allows you to identify and destroys targets much faster.
- Junkist upgrade: Simple bomb (90 MP, 110 ATK) -> Artisanal nail bomb (100 MP, 170 ATK): You laugh maniacally as you tape a kilogram of nails on your bombs, rendering them much more lethal.
- Warrior upgrade: Knee-breaking slam (250 MP, 200 ATK) -> Rib-breaking slam (280 MP, 300 ATK): Your vision slowly becomes redder as you enhance your signature move, being able to break through much more bone than ever before.
- Paladin upgrade: Holy beam (210 MP, 180 ATK) -> Ultra divine beam (230 MP, 250 ATK): Your holy powers increase twofold as you chant to the skies, full of glory. Your sword shines a powerful shade of gold as you prepare to strike your enemies harder than ever before.
- Washing machine monster upgrade: Water thrash (350 MP, 400 ATK) -> Destructive whirlpool (370 MP, 480 ATK): You start spinning at a rushed rate, vibrating with rage. Any enemy that tumbles inside you will be met with a powerful whirlpool which will tear their flesh apart.
- Giant upgrade: Wall-breaking kick (800 MP, 1000 ATK) -> Earth-shattering kick (850 MP, 1200 ATK): Your legs start mutating and your leg muscles grotesquely increase in size. You can now kick enemies with a much stronger force.
- """
    }
    }
}