
# Import necessary libraries
import requests  # Used for making HTTP requests
import json  # Used for working with JSON data

elevenlabs_api_key = open("token_elevenlabs").read().strip()
voices = {
    "jerma": "K8HtDE9RkBCs60Rk6pp4",
    "celestia": "Zze42DHnmcx6SZAtWKGN",
    "wheatley": "xs4GCyMlVRQ9chxVFkgM",
    "junko_queen": "Narstwn9H0fhqxU5lrhr",
    "junko": "2hjiKUqnZ6xafxUhbyg1",
    "monokuma": "uQLrUn9FlTrhoSqxlA5e",
    "nagito": "WzKwWeBabTgJgp0FJoJO"
}

# from elevenlabs import ElevenLabs

# client = ElevenLabs(
#     api_key = elevenlabs_api_key,
# )

# print(client.models.get_all())

def text_to_speech(text, output_file, voice):
    print("text to speech", text)

    CHUNK_SIZE = 1024
    VOICE_ID = voices[voice] # ID of the voice model to use
    XI_API_KEY = elevenlabs_api_key  # Your API key for authentication

    url = f"https://api.elevenlabs.io/v1/text-to-speech/{VOICE_ID}"

    headers = {
        "Accept": "audio/mpeg",
        "Content-Type": "application/json",
        "xi-api-key": XI_API_KEY
    }

    data = {
        "text": text,
        "model_id": "eleven_multilingual_v2",
        "voice_settings": {
            "stability": 0.35,
            "similarity_boost": 0.99,
            "style": 0.42,
            "use_speaker_boost": True
        }
    }

    response = requests.post(url, json=data, headers=headers)
    with open(output_file, 'wb') as f:
        for chunk in response.iter_content(chunk_size=CHUNK_SIZE):
            if chunk:
                f.write(chunk)

def speech_to_speech(input_file, output_file, voice):
    print("converting voice to", voice)
    # Define constants for the script
    CHUNK_SIZE = 1024  # Size of chunks to read/write at a time
    XI_API_KEY = elevenlabs_api_key  # Your API key for authentication
    VOICE_ID = voices[voice] # ID of the voice model to use
    AUDIO_FILE_PATH = input_file  # Path to the input audio file
    OUTPUT_PATH = output_file # Path to save the output audio file

    # Construct the URL for the Speech-to-Speech API request
    sts_url = f"https://api.elevenlabs.io/v1/speech-to-speech/{VOICE_ID}/stream"

    # Set up headers for the API request, including the API key for authentication
    headers = {
        "Accept": "application/json",
        "xi-api-key": XI_API_KEY
    }

    # Set up the data payload for the API request, including model ID and voice settings
    # Note: voice settings are converted to a JSON string
    data = {
        "model_id": "eleven_multilingual_sts_v2",
        "voice_settings": json.dumps({
            "stability": 0.35,
            "similarity_boost": 0.99,
            "style": 0.42,
            "use_speaker_boost": True
        })
    }

    # Set up the files to send with the request, including the input audio file
    files = {
        "audio": open(AUDIO_FILE_PATH, "rb")
    }

    # Make the POST request to the STS API with headers, data, and files, enabling streaming response
    response = requests.post(sts_url, headers=headers, data=data, files=files, stream=True)

    # Check if the request was successful
    if response.ok:
        # Open the output file in write-binary mode
        with open(OUTPUT_PATH, "wb") as f:
            # Read the response in chunks and write to the file
            for chunk in response.iter_content(chunk_size=CHUNK_SIZE):
                f.write(chunk)
        # Inform the user of success
        print("Audio stream saved successfully.")
    else:
        # Print the error message if the request was not successful
        print(response.text)